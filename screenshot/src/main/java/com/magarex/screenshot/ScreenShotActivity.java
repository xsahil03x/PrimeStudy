package com.magarex.screenshot;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.Image;
import android.media.ImageReader;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class ScreenShotActivity extends Activity {

    private static final String TAG = "ScreenShotActivity";
    private ImageReader mImageReader;
    private MediaProjectionManager mMediaProjectionManager;
    private Intent mResultData;
    private int mResultCode;
    private MediaProjection mMediaProjection;
    private int mScreenDensity;
    private VirtualDisplay mVirtualDisplay;
    private Display mDisplay;
    private int mWidth;
    private int mHeight;
    private Handler mHandler;

    private int top, bottom, right, left;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        mDisplay = getWindowManager().getDefaultDisplay();
        mDisplay.getMetrics(displayMetrics);
        mScreenDensity = displayMetrics.densityDpi;

        new Thread() {
            @Override
            public void run() {
                Looper.prepare();
                mHandler = new Handler();
                Looper.loop();
            }
        }.start();

        if (getIntent() != null) {
            top = (int) getIntent().getFloatExtra("top", 0.0f);
            left = (int) getIntent().getFloatExtra("left", 0.0f);
            right = (int) getIntent().getFloatExtra("right", 0.0f);
            bottom = (int) getIntent().getFloatExtra("bottom", 0.0f);
        }

        LocalBroadcastManager.getInstance(ScreenShotActivity.this).sendBroadcast(new Intent("FLOYDWIZ_ACTION_SCREENSHOT_COMPLETED"));

        mMediaProjectionManager = (MediaProjectionManager) getSystemService(Context.MEDIA_PROJECTION_SERVICE);
        startActivityForResult(mMediaProjectionManager.createScreenCaptureIntent(), 213);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 213) {
            if (resultCode != Activity.RESULT_OK) {
                Toast.makeText(this, "User denied screen capture permission", Toast.LENGTH_SHORT).show();
                return;
            }

            mResultCode = resultCode;
            mResultData = data;
            setUpMediaProjection();
            captureScreen();
        }
    }

    private void captureScreen() {
        if (mImageReader != null) {
            mVirtualDisplay = mMediaProjection.createVirtualDisplay("screen-shot", mWidth, mHeight,
                    mScreenDensity, DisplayManager.VIRTUAL_DISPLAY_FLAG_PUBLIC | DisplayManager.VIRTUAL_DISPLAY_FLAG_OWN_CONTENT_ONLY,
                    mImageReader.getSurface(), null, mHandler);
        }
    }

    private void setUpMediaProjection() {
        Point point = new Point();
        mDisplay.getRealSize(point);
        mWidth = point.x;
        mHeight = point.y;
        Log.i(TAG, "Width: " + mWidth + ", mHeight: " + mHeight);

        mMediaProjection = mMediaProjectionManager.getMediaProjection(mResultCode, mResultData);

        mImageReader = ImageReader.newInstance(mWidth, mHeight, PixelFormat.RGBA_8888, 2);
        mImageReader.setOnImageAvailableListener(new ImageReader.OnImageAvailableListener() {
            @Override
            public void onImageAvailable(ImageReader reader) {
                Image image = null;
                FileOutputStream fos = null;
                Bitmap bitmap = null;
                try {
                    image = reader.acquireLatestImage();
                    if (image != null) {
                        Image.Plane[] planes = image.getPlanes();
                        if (planes[0].getBuffer() == null) {
                            return;
                        }
                        int width = image.getWidth();
                        int height = image.getHeight();
                        int pixelStride = planes[0].getPixelStride();
                        int rowStride = planes[0].getRowStride();
                        int rowPadding = rowStride - pixelStride * mWidth;

                        Log.i(TAG, "imageWidth: " + width + ", imageHeight: " + height);
                        ByteBuffer buffer = planes[0].getBuffer();
                        bitmap = Bitmap.createBitmap(width + rowPadding / pixelStride, height, Bitmap.Config.ARGB_8888);
                        bitmap.copyPixelsFromBuffer(buffer);

                        Bitmap croppedBitmap = Bitmap.createBitmap(bitmap, left, top, right - left, bottom - top);
                        File file = new File(Environment.getExternalStorageDirectory() + "/test" + ".jpeg");

                        file.getParentFile().mkdirs();
                        file.createNewFile();
                        fos = new FileOutputStream(file);
                        croppedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                        Log.i(TAG, "onImageAvailable: ");
                        stopProjection();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (fos != null) {
                        try {
                            fos.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    if (bitmap != null) {
                        bitmap.recycle();
                    }

                    if (image != null) {
                        image.close();
                    }
                }
            }
        }, mHandler);
    }

    private void stopProjection() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mVirtualDisplay == null) {
                    return;
                }

                mVirtualDisplay.release();
                mVirtualDisplay = null;
                if (mImageReader != null) {
                    mImageReader.setOnImageAvailableListener(null, null);
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                });
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        tearDownMediaProjection();
    }

    private void tearDownMediaProjection() {
        if (mMediaProjection != null) {
            mMediaProjection.stop();
            mMediaProjection = null;
        }
    }
}

package com.magarex.screenshot;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

public class ScreenShot extends View {
    private Context mContext;
    private PointF beginCoordinate = new PointF(0.0f, 0.0f);
    private PointF endCoordinate = new PointF(0.0f, 0.0f);
    private Paint fillPaint = new Paint();
    private Paint strokePaint = new Paint();
    private float top, bottom, right, left;

    private static final String TAG = "ScreenShot";

    public ScreenShot(Context context) {
        super(context);
        init(context);
    }

    public ScreenShot(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ScreenShot(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        this.mContext = context;
        fillPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        fillPaint.setStyle(Paint.Style.FILL);

        float[] intervals = new float[]{10.0f, 10.0f};
        float phase = 0;
        strokePaint.setStyle(Paint.Style.STROKE);
        strokePaint.setStrokeWidth(getResources().getDimensionPixelSize(R.dimen.rect_capture_stroke_width));
        strokePaint.setColor(ContextCompat.getColor(context, android.R.color.darker_gray));
        DashPathEffect dashPathEffect = new DashPathEffect(intervals, phase);
        strokePaint.setPathEffect(dashPathEffect);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                beginCoordinate.x = event.getX();
                beginCoordinate.y = event.getY();
                endCoordinate.x = event.getX();
                endCoordinate.y = event.getY();
                invalidate();
                return true;
            case MotionEvent.ACTION_MOVE:
                Log.i(TAG, "onTouchEvent: " + event.getX() + " " + event.getY());
                endCoordinate.x = event.getX();
                endCoordinate.y = event.getY();
                invalidate();
                return true;
            case MotionEvent.ACTION_UP:
                invalidate();
                captureScreenShot();
                return true;
            default:
                return false;
        }
    }

    private void captureScreenShot() {
        Intent intent = new Intent(mContext, ScreenShotActivity.class);
        intent.putExtra("top", top);
        intent.putExtra("bottom", bottom);
        intent.putExtra("right", right);
        intent.putExtra("left", left);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawARGB(128, 0, 0, 0);
        left = beginCoordinate.x < endCoordinate.x ? beginCoordinate.x : endCoordinate.x;
        top = beginCoordinate.y < endCoordinate.y ? beginCoordinate.y : endCoordinate.y;
        right = beginCoordinate.x > endCoordinate.x ? beginCoordinate.x : endCoordinate.x;
        bottom = beginCoordinate.y > endCoordinate.y ? beginCoordinate.y : endCoordinate.y;
        canvas.drawRect(left, top, right, bottom, fillPaint);
        canvas.drawRect(left, top, right, bottom, strokePaint);
    }
}

/*
 * Copyright 2018 Magarex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.floydwiz.shared;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;

import java.util.Objects;

/**
 * Created by sahil on 26/12/18.
 **/
public class ClipBoardManager {
    private Context mContext;
    private String retrievedData;
    private static ClipboardManager clipboard;

    public ClipBoardManager(Context mContext) {
        this.mContext = mContext;
    }

    public String getRetrievedData() {
        clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
        if (clipboard != null) {
            if (!clipboard.hasPrimaryClip())
                retrievedData = "Oops nothing copied";
            else {
                //since the clipboard contains plain text.
                ClipData.Item item = Objects.requireNonNull(clipboard.getPrimaryClip()).getItemAt(0);
                // Gets the clipboard as text.
                retrievedData = item.getText().toString();
            }
        }
        return retrievedData;
    }

    public static ClipboardManager getClipBoardInstance(Context mContext) {
        clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
        return clipboard;
    }
}
/*
 * Copyright 2018 DevCompat
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.floydwiz.quicksearch.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.floydwiz.quicksearch.R;
import com.floydwiz.quicksearch.databinding.LayoutQuickLookItemOxfordItemBinding;
import com.floydwiz.quicksearch.model.oxford.Example;
import com.floydwiz.quicksearch.model.oxford.MeaningModel;
import com.floydwiz.quicksearch.model.oxford.Sense;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

class OxfordViewHolder extends RecyclerView.ViewHolder {

    final LayoutQuickLookItemOxfordItemBinding mBinding;

    OxfordViewHolder(LayoutQuickLookItemOxfordItemBinding itemBinding) {
        super(itemBinding.getRoot());
        this.mBinding = itemBinding;
    }
}

public class OxfordAdapter extends RecyclerView.Adapter<OxfordViewHolder> {

    private final Context mContext;
    private List<MeaningModel> mMeanings;
    private StringBuilder def, exam;

    public OxfordAdapter(Context context) {
        this.mContext = context;
        def = new StringBuilder();
        exam = new StringBuilder();
    }

    public void addOxfordToList(List<MeaningModel> resultList) {
        this.mMeanings = resultList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public OxfordViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutQuickLookItemOxfordItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(mContext),
                R.layout.layout_quick_look_item_oxford_item, parent, false);
        return new OxfordViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final OxfordViewHolder holder, int position) {
        holder.mBinding.setMeaningModel(mMeanings.get(position));
        if (mMeanings.get(position).getSenseList() != null && mMeanings.get(
                position).getSenseList().size() > 0) {
            def.delete(0, def.length());
            exam.delete(0, exam.length());
            for (Sense data : mMeanings.get(position).getSenseList()) {
                if (data.definitions != null && data.definitions.size() > 0) {
                    for (String definition : data.definitions) {
                        if (!definition.isEmpty()) {
                            def.append("\u2022 ").append(definition).append("\n");
                        }
                    }
                    holder.mBinding.tvDefinition.setText(def);
                }
                if (data.examples != null && data.examples.size() > 0) {
                    for (Example example : data.examples) {
                        if (!example.text.isEmpty()) {
                            exam.append("\u2022 ").append(example.text).append("\n");
                        }
                    }
                    holder.mBinding.tvExample.setText(exam);
                }
            }
        }

        if (def.length() == 0) {
            holder.mBinding.tvDefinition.setVisibility(View.GONE);
        } else {
            holder.mBinding.tvDefinition.setVisibility(View.VISIBLE);
        }
        if (exam.length() == 0) {
            holder.mBinding.tvExample.setVisibility(View.GONE);
            holder.mBinding.tvExampleBold.setVisibility(View.GONE);
        } else {
            holder.mBinding.tvExample.setVisibility(View.VISIBLE);
            holder.mBinding.tvExampleBold.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        if (mMeanings == null) {
            return 0;
        } else {
            return mMeanings.size();
        }
    }

}

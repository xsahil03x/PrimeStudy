/*
 * Copyright 2018 DevCompat
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.floydwiz.quicksearch.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.floydwiz.quicksearch.ui.GoogleSearchFragment;
import com.floydwiz.quicksearch.ui.OxfordFragment;
import com.floydwiz.quicksearch.ui.WikiFragment;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;


public class ViewPagerAdapter extends PagerAdapter {
    private Context mContext;

    public ViewPagerAdapter(Context context) {
        super();
        mContext = context;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        View view = null;
        switch (position) {
            case 0:
                view = new OxfordFragment().getView(mContext, container);
                break;
            case 1:
                view = WikiFragment.getView(mContext, container);
                break;
            case 2:
                view = GoogleSearchFragment.getView(mContext, container);
                break;
        }

        container.addView(view);
        return view;
    }
}

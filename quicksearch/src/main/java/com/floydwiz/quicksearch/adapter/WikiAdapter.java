/*
 * Copyright 2018 DevCompat
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.floydwiz.quicksearch.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.floydwiz.quicksearch.R;
import com.floydwiz.quicksearch.databinding.LayoutQuickLookItemWikiItemBinding;
import com.floydwiz.quicksearch.model.wiki.WikiOpenSearch;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

class WikiViewHolder extends RecyclerView.ViewHolder {

    final LayoutQuickLookItemWikiItemBinding mBinding;

    WikiViewHolder(LayoutQuickLookItemWikiItemBinding itemBinding) {
        super(itemBinding.getRoot());
        this.mBinding = itemBinding;
    }
}

public class WikiAdapter extends RecyclerView.Adapter<WikiViewHolder> {

    private final Context mContext;
    private List<WikiOpenSearch> mSearchList;

    public WikiAdapter(Context context) {
        this.mContext = context;
    }

    public void addWikiToList(List<WikiOpenSearch> resultList) {
        this.mSearchList = resultList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public WikiViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutQuickLookItemWikiItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(mContext),
                R.layout.layout_quick_look_item_wiki_item, parent, false);
        return new WikiViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull final WikiViewHolder holder, int position) {
        holder.mBinding.setWikiOpenSearch(mSearchList.get(position));
        holder.mBinding.textView4.setText("\u2022 "+mSearchList.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        if (mSearchList == null) {
            return 0;
        } else {
            return mSearchList.size();
        }
    }

}

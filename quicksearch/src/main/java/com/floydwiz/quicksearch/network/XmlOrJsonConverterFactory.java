/*
 * Copyright 2018 DevCompat
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.floydwiz.quicksearch.network;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Type;

import androidx.annotation.Nullable;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;


public class XmlOrJsonConverterFactory extends Converter.Factory {

    @Retention(RetentionPolicy.RUNTIME)
    @interface Xml {
    }

    @Retention(RetentionPolicy.RUNTIME)
    @interface Json {
    }

    private final Converter.Factory xml = SimpleXmlConverterFactory.create();
    private final Converter.Factory json = GsonConverterFactory.create();

    @Nullable
    @Override
    public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
        for (Annotation annotation : annotations) {
            if (annotation instanceof Xml) {
                return xml.responseBodyConverter(type, annotations, retrofit);
            }
            if (annotation instanceof Json) {
                return json.responseBodyConverter(type, annotations, retrofit);
            }
        }
        return null;
    }
}

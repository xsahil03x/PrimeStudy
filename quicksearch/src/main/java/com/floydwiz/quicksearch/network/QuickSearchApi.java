/*
 * Copyright 2018 DevCompat
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.floydwiz.quicksearch.network;

import com.floydwiz.quicksearch.model.oxford.OxfordModelResponse;
import com.floydwiz.quicksearch.model.wiki.WikiOpenSearchResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface QuickSearchApi {

    @XmlOrJsonConverterFactory.Xml
    @Headers("Accept: application/xml")
    @GET("https://en.wikipedia.org/w/api.php?action=opensearch&format=xml")
    Call<WikiOpenSearchResponse> getWikiDataByOpenSearch(@Query("search") String search);

    @XmlOrJsonConverterFactory.Json
    @Headers({"app_id: adc0285b",
            "app_key: 8135a43940bb5877b79b8c127bc1c99c",
            "Accept: application/json"})
    @GET("https://od-api.oxforddictionaries.com/api/v1/entries/en/{wordId}")
    Call<OxfordModelResponse> getWordDetails(@Path("wordId") String wordId);

}

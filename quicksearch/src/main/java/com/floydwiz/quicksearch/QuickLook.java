/*
 * Copyright 2018 DevCompat
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.floydwiz.quicksearch;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.floydwiz.quicksearch.adapter.ViewPagerAdapter;
import com.google.android.material.tabs.TabLayout;

import java.util.Objects;

import androidx.viewpager.widget.ViewPager;


public class QuickLook {

    private Context mContext;
    private View view;
    private int[] mFragmentIconList = {
            R.drawable.ic_a_z,
            R.drawable.ic_wikipedia,
            R.drawable.ic_google_search
    };

    public QuickLook(Context context) {
        mContext = context;
    }

    public View getView() {
        view = LayoutInflater.from(mContext).inflate(R.layout.layout_quick_look, null);
        setUpToolbar();
        return view;
    }

    private void setUpToolbar() {
        ViewPager viewPager = view.findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(2);
        ViewPagerAdapter adapter = new ViewPagerAdapter(mContext);
        viewPager.setAdapter(adapter);
        TabLayout tabs = view.findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager, true);
        setupTabItems(tabs);
    }

    private void setupTabItems(TabLayout tabs) {
        View root = tabs.getChildAt(0);
        if (root instanceof LinearLayout) {
            ((LinearLayout) root).setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
            GradientDrawable drawable = new GradientDrawable();
            drawable.setColor(mContext.getResources().getColor(R.color.grey_brown));
            drawable.setSize(1, 2);
            ((LinearLayout) root).setDividerPadding(10);
            ((LinearLayout) root).setDividerDrawable(drawable);
        }
        for (int i = 0; i < tabs.getTabCount(); i++) {
            Objects.requireNonNull(tabs.getTabAt(i)).setIcon(mFragmentIconList[i]);
        }
    }
}

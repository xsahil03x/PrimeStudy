/*
 * Copyright 2018 DevCompat
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.floydwiz.quicksearch.model.oxford;


import java.util.List;

public class MeaningModel {

    private String lexicalCategory;
    private String audioFile;
    private String phoneticWord;
    private List<Sense> senseList;
    private String meaningWord;

    public MeaningModel() {

    }

    public MeaningModel(String lexicalCategory, String audioFile, String phoneticWord,
                        List<Sense> senseList, String meaningWord) {
        this.lexicalCategory = lexicalCategory;
        this.audioFile = audioFile;
        this.phoneticWord = phoneticWord;
        this.senseList = senseList;
        this.meaningWord = meaningWord;
    }

    public void setLexicalCategory(String lexicalCategory) {
        this.lexicalCategory = lexicalCategory;
    }

    public void setAudioFile(String audioFile) {
        this.audioFile = audioFile;
    }

    public void setPhoneticWord(String phoneticWord) {
        this.phoneticWord = phoneticWord;
    }

    public void setSenseList(List<Sense> senseList) {
        this.senseList = senseList;
    }

    public String getLexicalCategory() {
        return lexicalCategory;
    }

    public String getAudioFile() {
        return audioFile;
    }

    public String getPhoneticWord() {
        return phoneticWord;
    }

    public List<Sense> getSenseList() {
        return senseList;
    }

    public String getMeaningWord() {
        return meaningWord;
    }

    public void setMeaningWord(String meaningWord) {
        this.meaningWord = meaningWord;
    }

    public String getNoDataAvailable() {
        return "No Data";
    }
}

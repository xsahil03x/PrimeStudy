/*
 * Copyright 2018 DevCompat
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.floydwiz.quicksearch.model.wiki;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "Item")
public class WikiOpenSearch {

    @Element(name = "Text")
    private String Text;

    @Element(name = "Url")
    private String Url;

    @Element(name = "Description")
    private String Description;

    @Element(name = "Image", required = false)
    private WikiImage Image;

    public WikiImage getImage() {
        return Image;
    }

    public String getText() {
        return Text;
    }

    public String getUrl() {
        return Url;
    }

    public String getDescription() {
        return Description;
    }
}

/*
 * Copyright 2018 DevCompat
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.floydwiz.quicksearch.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.floydwiz.quicksearch.R;
import com.floydwiz.quicksearch.databinding.LayoutQuickLookItemGoogleFragmentBinding;
import com.floydwiz.quicksearch.utils.Helpers;
import com.floydwiz.shared.ClipBoardManager;

import androidx.databinding.DataBindingUtil;

import static com.floydwiz.quicksearch.utils.Helpers.GOOGLE_SEARCH_BASE_URL;


public class GoogleSearchFragment {

    private static final String TAG = "GoogleSearchFragment";

    public GoogleSearchFragment() {
    }

    @SuppressLint({"SetJavaScriptEnabled", "ClickableViewAccessibility"})
    public static View getView(Context context, ViewGroup container) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LayoutQuickLookItemGoogleFragmentBinding mBinding = DataBindingUtil.inflate(inflater, R.layout.layout_quick_look_item_google_fragment, container, false);
        ClipBoardManager clipBoardManager = new ClipBoardManager(context);
        String SearchQuery = clipBoardManager.getRetrievedData();
        String finalUrl = GOOGLE_SEARCH_BASE_URL + SearchQuery;

        WebView wbGoogleSearch = mBinding.wbGoogleSearch;
        wbGoogleSearch.getSettings().setJavaScriptEnabled(true);
        wbGoogleSearch.setWebChromeClient(new WebChromeClient());
        wbGoogleSearch.getSettings().setLoadWithOverviewMode(true);
        wbGoogleSearch.getSettings().setUseWideViewPort(true);
        if (clipBoardManager.getRetrievedData().equals("Oops nothing copied")) {
            wbGoogleSearch.loadUrl(Helpers.GOOGLE_BASE_URL);
        } else {
            wbGoogleSearch.loadUrl(finalUrl);
        }
        wbGoogleSearch.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                Log.i(TAG, "onPageFinished: load finished " + url);
            }
        });

        return mBinding.getRoot();
    }
}

/*
 * Copyright 2018 DevCompat
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.floydwiz.quicksearch.ui;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.floydwiz.quicksearch.R;
import com.floydwiz.quicksearch.adapter.OxfordAdapter;
import com.floydwiz.quicksearch.databinding.LayoutQuickLookItemOxfordBinding;
import com.floydwiz.quicksearch.model.oxford.LexicalEntry;
import com.floydwiz.quicksearch.model.oxford.MeaningModel;
import com.floydwiz.quicksearch.model.oxford.OxfordModelResponse;
import com.floydwiz.quicksearch.network.RetrofitBuilder;
import com.floydwiz.shared.ClipBoardManager;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class OxfordFragment {

    private static final String TAG = "OxfordFragment";
    private List<MeaningModel> mMeaningList = new ArrayList<>();
    private OxfordAdapter mAdapter;
    private String SearchQuery;

    public OxfordFragment() {
    }

    public View getView(Context context, ViewGroup container) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LayoutQuickLookItemOxfordBinding mBinding = DataBindingUtil.inflate(inflater, R.layout.layout_quick_look_item_oxford, container, false);
        ClipBoardManager clipBoardManager = new ClipBoardManager(context);
        SearchQuery = clipBoardManager.getRetrievedData();
        ProgressBar progressBar = mBinding.progressBar;
        progressBar.animate();

        RecyclerView recyclerView = mBinding.rvOxford;
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));

        mAdapter = new OxfordAdapter(context);
        recyclerView.setAdapter(mAdapter);

        RetrofitBuilder.getClient().getWordDetails(SearchQuery).enqueue(new Callback<OxfordModelResponse>() {
            @Override
            public void onResponse(@NonNull Call<OxfordModelResponse> call, @NonNull Response<OxfordModelResponse> response) {
                if (response.isSuccessful() && !response.body().results.isEmpty()) {
                    progressBar.setVisibility(View.GONE);
                    parseDictionaryData(response.body().results.get(0).lexicalEntries);
                } else {
                    Log.i(TAG, "onResponse: " + response.errorBody());
                    mBinding.rvOxford.setVisibility(View.GONE);
                    mBinding.progressBar.setVisibility(View.GONE);
                    mBinding.tvNoResult.setVisibility(View.VISIBLE);
                    mBinding.ivNoResult.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(@NonNull Call<OxfordModelResponse> call, @NonNull Throwable t) {
                Log.i(TAG, "onFailure: " + t.getMessage());
                mBinding.rvOxford.setVisibility(View.GONE);
                mBinding.progressBar.setVisibility(View.GONE);
                mBinding.tvNoResult.setVisibility(View.VISIBLE);
                mBinding.ivNoResult.setVisibility(View.VISIBLE);
            }
        });

        return mBinding.getRoot();
    }

    private int dpToPx(Context context) {
        Resources r = context.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, r.getDisplayMetrics()));
    }


    private void parseDictionaryData(List<LexicalEntry> results) {
        if (results != null) {
            mMeaningList.clear();
            for (LexicalEntry lexicalEntry : results) {
                MeaningModel meaningModel = new MeaningModel();
                meaningModel.setLexicalCategory(lexicalEntry.lexicalCategory);
                if (lexicalEntry.entries != null && lexicalEntry.entries.size() > 0) {
                    meaningModel.setSenseList(lexicalEntry.entries.get(0).senses);
                }
                if (lexicalEntry.pronunciations != null && lexicalEntry.pronunciations.size() > 0) {
                    meaningModel.setAudioFile(lexicalEntry.pronunciations.get(0).audioFile);
                    meaningModel.setPhoneticWord(lexicalEntry.pronunciations.get(0).phoneticSpelling);
                }
                meaningModel.setMeaningWord(SearchQuery);
                mMeaningList.add(meaningModel);
            }
            mAdapter.addOxfordToList(mMeaningList);
        }
    }
}

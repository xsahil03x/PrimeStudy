/*
 * Copyright 2018 DevCompat
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.floydwiz.quicksearch.ui;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.floydwiz.quicksearch.R;
import com.floydwiz.quicksearch.adapter.WikiAdapter;
import com.floydwiz.quicksearch.databinding.LayoutQuickLookItemWikiBinding;
import com.floydwiz.quicksearch.model.wiki.WikiOpenSearch;
import com.floydwiz.quicksearch.model.wiki.WikiOpenSearchResponse;
import com.floydwiz.quicksearch.network.RetrofitBuilder;
import com.floydwiz.shared.ClipBoardManager;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WikiFragment {

    private static final String TAG = "WikiFragment";

    public WikiFragment() {
    }

    public static View getView(Context context, ViewGroup container) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LayoutQuickLookItemWikiBinding mBinding = DataBindingUtil.inflate(inflater, R.layout.layout_quick_look_item_wiki, container, false);
        ClipBoardManager clipBoardManager = new ClipBoardManager(context);
        RecyclerView rvWiki = mBinding.rvWiki;
        WikiAdapter mAdapter = new WikiAdapter(context);
        rvWiki.setLayoutManager(new LinearLayoutManager(context));
        rvWiki.setItemAnimator(new DefaultItemAnimator());
        rvWiki.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));
        rvWiki.setAdapter(mAdapter);
        String SearchQuery = clipBoardManager.getRetrievedData();
        RetrofitBuilder.getClient().getWikiDataByOpenSearch(SearchQuery).enqueue(new Callback<WikiOpenSearchResponse>() {
            @Override
            public void onResponse(@NonNull Call<WikiOpenSearchResponse> call, @NonNull Response<WikiOpenSearchResponse> response) {
                if (response.isSuccessful() && !response.body().getItemList().isEmpty()) {
                    Log.i(TAG, "onResponse: " + response.body().getItemList().get(0).getDescription());
                    List<WikiOpenSearch> wikiOpenSearch = response.body().getItemList();
                    mAdapter.addWikiToList(wikiOpenSearch);
                    mBinding.progressBar.setVisibility(View.GONE);
                } else {
                    Log.i(TAG, "onResponse: " + response.errorBody());
                    mBinding.rvWiki.setVisibility(View.GONE);
                    mBinding.progressBar.setVisibility(View.GONE);
                    mBinding.tvNoResult.setVisibility(View.VISIBLE);
                    mBinding.ivNoResult.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<WikiOpenSearchResponse> call, Throwable t) {
                Log.i(TAG, "onFailure: " + t.getCause().toString());
                mBinding.rvWiki.setVisibility(View.GONE);
                mBinding.progressBar.setVisibility(View.GONE);
                mBinding.tvNoResult.setVisibility(View.VISIBLE);
                mBinding.ivNoResult.setVisibility(View.VISIBLE);
            }
        });
        return mBinding.getRoot();
    }
}

/*
 * Copyright 2018 DevCompat
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.magarex.flashcard.adapter;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.magarex.flashcard.R;
import com.magarex.flashcard.listener.OnArchiveClickListener;
import com.magarex.flashcard.listener.OnDeleteClickListener;
import com.magarex.flashcard.model.FlashCardModel;
import com.magarex.flashcard.utils.Status;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import eu.davidea.flipview.FlipView;

class FlashCardViewHolder extends RecyclerView.ViewHolder {

    FlipView cardFlipView;
    TextView cardQuestion;
    TextView cardAnswer;
    ImageView deleteNote;
    ImageView archiveNote;

    FlashCardViewHolder(View itemView) {
        super(itemView);
        this.cardFlipView = itemView.findViewById(R.id.cardFlipView);
        this.cardQuestion = itemView.findViewById(R.id.tvCardQuestion);
        this.cardAnswer = itemView.findViewById(R.id.tvCardAnswer);
        this.deleteNote = itemView.findViewById(R.id.imvDeleteCard);
        this.archiveNote = itemView.findViewById(R.id.imvArchiveCard);
    }
}

public class FlashCardAdapter extends RecyclerView.Adapter<FlashCardViewHolder> {

    private Context mContext;
    private List<FlashCardModel> mCards;
    private Status listType;
    private OnDeleteClickListener onDeleteClickListener;
    private OnArchiveClickListener onArchiveClickListener;

    public FlashCardAdapter(Context context, Status listType, OnDeleteClickListener onDeleteClickListener, OnArchiveClickListener onArchiveClickListener) {
        this.mContext = context;
        this.listType = listType;
        this.onDeleteClickListener = onDeleteClickListener;
        this.onArchiveClickListener = onArchiveClickListener;
    }

    public void addCardsToList(List<FlashCardModel> flashCardModelList) {
        this.mCards = flashCardModelList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public FlashCardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_show_flash_card_item, parent, false);
        FlashCardViewHolder flashCardViewHolder = new FlashCardViewHolder(view);
        flashCardViewHolder.deleteNote.setOnClickListener(v -> onDeleteClickListener.onDeleteCard(mCards.get(flashCardViewHolder.getAdapterPosition()).getId()));
        flashCardViewHolder.archiveNote.setOnClickListener(v -> onArchiveClickListener.onArchiveCard(mCards.get(flashCardViewHolder.getAdapterPosition()).getId()));
        return flashCardViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull FlashCardViewHolder holder, int position) {
        holder.cardQuestion.setText(mCards.get(position).getQuestion());
        holder.cardAnswer.setText(mCards.get(position).getAnswer());
        GradientDrawable mDrawable = (GradientDrawable) mContext.getResources().getDrawable(R.drawable.background_gradient_front);
        mDrawable.setColor(mCards.get(position).getBackgroundColor());
        holder.cardFlipView.setChildBackgroundDrawable(0, mDrawable);
        holder.archiveNote.setRotation(listType != Status.ACTIVE ? 180 : 0);
    }

    @Override
    public int getItemCount() {
        if (mCards == null) {
            return 0;
        } else {
            return mCards.size();
        }
    }
}

package com.magarex.flashcard.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.floydwiz.shared.AppUtils;
import com.google.android.material.snackbar.Snackbar;
import com.magarex.flashcard.R;
import com.magarex.flashcard.adapter.FlashCardAdapter;
import com.magarex.flashcard.database.FlashCardDatabase;
import com.magarex.flashcard.listener.OnArchiveClickListener;
import com.magarex.flashcard.listener.OnDeleteClickListener;
import com.magarex.flashcard.utils.GridSpacingItemDecoration;
import com.magarex.flashcard.utils.Status;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by sahil on 4/1/19.
 **/
public class TrashView implements OnDeleteClickListener, OnArchiveClickListener {
    private Context mContext;
    private FlashCardDatabase mDatabase;
    private TextView tvEmptyList;
    private ImageView ivEmptyList;
    private RecyclerView rvTrash;
    private FlashCardAdapter mAdapter;
    private View view;
    private CompositeDisposable mDisposable;

    private static final String TAG = "ArchiveView";

    TrashView(Context context) {
        mContext = context;
        mDatabase = FlashCardDatabase.getInstance(mContext);
        mDisposable = new CompositeDisposable();
    }

    public View getView() {
        view = LayoutInflater.from(mContext).inflate(R.layout.layout_trash, null);
        tvEmptyList = view.findViewById(R.id.tvEmptyList);
        ivEmptyList = view.findViewById(R.id.ivEmptyList);
        rvTrash = view.findViewById(R.id.rvTrash);
        rvTrash.setLayoutManager(new LinearLayoutManager(mContext));
        rvTrash.addItemDecoration(new GridSpacingItemDecoration(1, AppUtils.dpToPx(4), true));
        mAdapter = new FlashCardAdapter(mContext, Status.TRASH, this, this);
        rvTrash.setAdapter(mAdapter);
        loadNotes();
        return view;
    }

    private void loadNotes() {
        mDisposable.add(mDatabase.flashCardDao().loadNotesByStatus(Status.TRASH)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cards -> {
                    if (cards.size() > 0)
                        mAdapter.addCardsToList(cards);
                    else {
                        rvTrash.setVisibility(View.GONE);
                        tvEmptyList.setVisibility(View.VISIBLE);
                        ivEmptyList.setVisibility(View.VISIBLE);
                    }
                }));
    }

    @Override
    public void onArchiveCard(int id) {
        // This will restore this note
        mDisposable.add(Completable.fromAction(() -> mDatabase.flashCardDao().updateStatus(Status.ACTIVE, id))
                .subscribeOn(Schedulers.io())
                .subscribe(() -> AppUtils.showBasicSnackBar(view, "Successfully Restored", Snackbar.LENGTH_SHORT)));
    }

    @Override
    public void onDeleteCard(int id) {
        mDisposable.add(Completable.fromAction(() -> mDatabase.flashCardDao().deleteCard(id))
                .subscribeOn(Schedulers.io())
                .subscribe(() -> AppUtils.showBasicSnackBar(view, "Deleted", Snackbar.LENGTH_SHORT)));
    }

    void clearAll() {
        mDisposable.clear();
    }

}

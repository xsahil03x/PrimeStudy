/*
 * Copyright 2018 DevCompat
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.magarex.flashcard.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.magarex.flashcard.R;
import com.magarex.flashcard.database.FlashCardDatabase;
import com.magarex.flashcard.listener.OnAddClickListener;
import com.magarex.flashcard.listener.OnSaveClickListener;
import com.magarex.flashcard.model.FlashCardModel;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class FlashCard implements OnAddClickListener, OnSaveClickListener, NavigationView.OnNavigationItemSelectedListener {

    private Context mContext;
    private View view;
    private ShowFlashCardView showFlashCardView;
    private AddFlashCardView addFlashCardView;
    private ArchiveView archiveView;
    private TrashView trashView;
    private FrameLayout flashCardView;
    private TextView tvToolbarTitle;
    private ImageView ivDrawerMenu, ivBack;
    private DrawerLayout mDrawerLayout;
    private FlashCardDatabase mDatabase;
    private CompositeDisposable mDisposable;

    public FlashCard(Context context) {
        mContext = context;
        mDisposable = new CompositeDisposable();
    }

    private static final String TAG = "FlashCard";

    public View getView() {
        view = LayoutInflater.from(mContext).inflate(R.layout.layout_flashcard, null);
        flashCardView = view.findViewById(R.id.frameFlashCardView);
        ivDrawerMenu = view.findViewById(R.id.ivDrawerMenu);
        ivBack = view.findViewById(R.id.ivBack);
        NavigationView navigationView = view.findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        tvToolbarTitle = view.findViewById(R.id.toolbar_title);
        mDrawerLayout = view.findViewById(R.id.drawerLayout);
        showFlashCardView = new ShowFlashCardView(mContext);
        addFlashCardView = new AddFlashCardView(mContext);
        archiveView = new ArchiveView(mContext);
        trashView = new TrashView(mContext);
        mDatabase = FlashCardDatabase.getInstance(mContext);
        addFlashCardView.setOnSaveClickListener(this);
        showFlashCardView.setOnAddClickListener(this);

        ivDrawerMenu.setOnClickListener(v -> mDrawerLayout.openDrawer(GravityCompat.START));
        flashCardView.addView(showFlashCardView.getView());

        ivBack.setOnClickListener(v -> {
            flashCardView.removeAllViews();
            flashCardView.addView(showFlashCardView.getView());
            changeToolbar(false);
        });

        return view;

    }

    private void changeToolbar(Boolean shouldChange) {
        if (shouldChange) {
            ivDrawerMenu.setVisibility(View.GONE);
            ivBack.setVisibility(View.VISIBLE);
        } else {
            ivDrawerMenu.setVisibility(View.VISIBLE);
            ivBack.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAdd() {
        flashCardView.removeAllViews();
        flashCardView.addView(addFlashCardView.getView());
        changeToolbar(true);
    }

    @Override
    public void onAdd(String question, String answer, int id) {

    }

    @Override
    public void onSave(FlashCardModel card) {
        if (card.getAnswer().isEmpty() || card.getQuestion().isEmpty()) {
            Toast.makeText(mContext, "Please fill in some details", Toast.LENGTH_SHORT).show();
        } else {
            mDisposable.add(Completable.fromAction(() -> mDatabase.flashCardDao().insertCard(card))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(() -> {
                        Toast.makeText(mContext, "Card Saved Successfully", Toast.LENGTH_SHORT).show();
                        showFlashCardView.clearAll();
                        flashCardView.removeAllViews();
                        flashCardView.addView(showFlashCardView.getView());
                        changeToolbar(false);
                    }));
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.menu_cards) {
            flashCardView.removeAllViews();
            flashCardView.addView(showFlashCardView.getView());
            tvToolbarTitle.setText("FlashCards");
        } else if (i == R.id.menu_archive) {
            flashCardView.removeAllViews();
            flashCardView.addView(archiveView.getView());
            tvToolbarTitle.setText("Archives");
        } else if (i == R.id.menu_trash) {
            flashCardView.removeAllViews();
            flashCardView.addView(trashView.getView());
            tvToolbarTitle.setText("Trash");
        }

        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
}

/*
 * Copyright 2018 DevCompat
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.magarex.flashcard.ui;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.floydwiz.shared.AppUtils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.magarex.flashcard.R;
import com.magarex.flashcard.adapter.FlashCardAdapter;
import com.magarex.flashcard.database.FlashCardDatabase;
import com.magarex.flashcard.listener.OnAddClickListener;
import com.magarex.flashcard.listener.OnArchiveClickListener;
import com.magarex.flashcard.listener.OnDeleteClickListener;
import com.magarex.flashcard.model.FlashCardModel;
import com.magarex.flashcard.utils.GridSpacingItemDecoration;
import com.magarex.flashcard.utils.Status;

import java.util.List;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class ShowFlashCardView implements OnDeleteClickListener, OnArchiveClickListener {

    private Context mContext;
    private View view;
    private RecyclerView rvFlashCard;
    private FlashCardAdapter mAdapter;
    private List<FlashCardModel> mCards;
    private FlashCardDatabase mDatabase;
    private TextView tvEmptyList;
    private ImageView ivEmptyList;
    private FloatingActionButton fabAddFlashCard;
    private CompositeDisposable mDisposable;


    private static final String TAG = "ShowFlashCardView";
    private OnAddClickListener onAddClickListener;

    ShowFlashCardView(Context context) {
        mContext = context;
        mDatabase = FlashCardDatabase.getInstance(mContext);
        mDisposable = new CompositeDisposable();
    }

    public View getView() {
        view = LayoutInflater.from(mContext).inflate(R.layout.layout_show_flash_card, null);
        rvFlashCard = view.findViewById(R.id.rvFlashCards);
        fabAddFlashCard = view.findViewById(R.id.fabAddCard);
        tvEmptyList = view.findViewById(R.id.tvEmptyList);
        ivEmptyList = view.findViewById(R.id.ivEmptyList);
        rvFlashCard.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));
        rvFlashCard.addItemDecoration(new GridSpacingItemDecoration(1, dpToPx(mContext), true));
        rvFlashCard.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new FlashCardAdapter(mContext, Status.ACTIVE, this, this);
        rvFlashCard.setAdapter(mAdapter);
        loadCards();
        fabAddFlashCard.setOnClickListener(v -> {
            if (onAddClickListener != null) {
                onAddClickListener.onAdd();
            }
        });
        return view;
    }

    private static int dpToPx(Context mContext) {
        Resources r = mContext.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6, r.getDisplayMetrics()));
    }


    public void setOnAddClickListener(OnAddClickListener onAddClickListener) {
        this.onAddClickListener = onAddClickListener;
    }

    private void loadCards() {
        mDisposable.add(mDatabase.flashCardDao().loadNotesByStatus(Status.ACTIVE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cards -> {
                    if (cards.size() > 0)
                        mAdapter.addCardsToList(cards);
                    else {
                        rvFlashCard.setVisibility(View.GONE);
                        tvEmptyList.setVisibility(View.VISIBLE);
                        ivEmptyList.setVisibility(View.VISIBLE);
                    }
                }));
    }

    @Override
    public void onDeleteCard(int id) {
        mDisposable.add(Completable.fromAction(() -> mDatabase.flashCardDao().updateStatus(Status.TRASH, id))
                .subscribeOn(Schedulers.io())
                .subscribe(() -> AppUtils.showBasicSnackBar(view, "Successfully Deleted", Snackbar.LENGTH_SHORT)));
    }

    @Override
    public void onArchiveCard(int id) {
        mDisposable.add(Completable.fromAction(() -> mDatabase.flashCardDao().updateStatus(Status.ARCHIVE, id))
                .subscribeOn(Schedulers.io())
                .subscribe(() -> AppUtils.showBasicSnackBar(view, "Successfully Restored", Snackbar.LENGTH_SHORT)));
    }

    void clearAll() {
        mDisposable.clear();
    }

}

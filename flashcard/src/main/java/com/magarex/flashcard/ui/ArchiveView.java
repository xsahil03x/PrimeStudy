package com.magarex.flashcard.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.floydwiz.shared.AppUtils;
import com.google.android.material.snackbar.Snackbar;
import com.magarex.flashcard.R;
import com.magarex.flashcard.adapter.FlashCardAdapter;
import com.magarex.flashcard.database.FlashCardDatabase;
import com.magarex.flashcard.listener.OnArchiveClickListener;
import com.magarex.flashcard.listener.OnDeleteClickListener;
import com.magarex.flashcard.utils.GridSpacingItemDecoration;
import com.magarex.flashcard.utils.Status;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by sahil on 4/1/19.
 **/
public class ArchiveView implements OnDeleteClickListener, OnArchiveClickListener {
    private Context mContext;
    private FlashCardDatabase mDatabase;
    private TextView tvEmptyList;
    private ImageView ivEmptyList;
    private View view;
    private RecyclerView rvArchives;
    private FlashCardAdapter mAdapter;
    private CompositeDisposable mDisposable;

    ArchiveView(Context context) {
        mContext = context;
        mDisposable = new CompositeDisposable();
        mDatabase = FlashCardDatabase.getInstance(mContext);
    }

    public View getView() {
        view = LayoutInflater.from(mContext).inflate(R.layout.layout_archive, null);
        tvEmptyList = view.findViewById(R.id.tvEmptyList);
        ivEmptyList = view.findViewById(R.id.ivEmptyList);
        rvArchives = view.findViewById(R.id.rvArchives);
        rvArchives.setLayoutManager(new LinearLayoutManager(mContext));
        rvArchives.addItemDecoration(new GridSpacingItemDecoration(1, AppUtils.dpToPx(4), true));
        mAdapter = new FlashCardAdapter(mContext, Status.ARCHIVE, this, this);
        rvArchives.setAdapter(mAdapter);
        loadNotes();
        return view;
    }

    private void loadNotes() {
        mDisposable.add(mDatabase.flashCardDao().loadNotesByStatus(Status.ARCHIVE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cards -> {
                    if (cards.size() > 0)
                        mAdapter.addCardsToList(cards);
                    else {
                        rvArchives.setVisibility(View.GONE);
                        tvEmptyList.setVisibility(View.VISIBLE);
                        ivEmptyList.setVisibility(View.VISIBLE);
                    }
                }));
    }

    @Override
    public void onArchiveCard(int id) {
        // This will unarchive this note
        mDisposable.add(Completable.fromAction(() -> mDatabase.flashCardDao().updateStatus(Status.ACTIVE, id))
                .subscribeOn(Schedulers.io())
                .subscribe(() -> AppUtils.showBasicSnackBar(view, "Successfully Restored", Snackbar.LENGTH_SHORT)));
    }

    @Override
    public void onDeleteCard(int id) {
        mDisposable.add(Completable.fromAction(() -> mDatabase.flashCardDao().updateStatus(Status.TRASH, id))
                .subscribeOn(Schedulers.io())
                .subscribe(() -> AppUtils.showBasicSnackBar(view, "Stored in trash", Snackbar.LENGTH_SHORT)));
    }

    void clearAll() {
        mDisposable.clear();
    }

}

/*
 * Copyright 2018 DevCompat
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.magarex.flashcard.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.magarex.flashcard.R;
import com.magarex.flashcard.listener.OnSaveClickListener;
import com.magarex.flashcard.model.FlashCardModel;
import com.magarex.flashcard.utils.RandomColorGenerator;


public class AddFlashCardView {

    private Context mContext;
    private View view;
    private EditText edCardQuestion, edCardAnswer;
    private FloatingActionButton fabSaveCard;
    private OnSaveClickListener onSaveClickListener;

    private static final String TAG = "AddFlashCardView";
    private int noteId = Integer.MIN_VALUE;

    AddFlashCardView(Context context) {
        mContext = context;
    }

    public View getView() {
        view = LayoutInflater.from(mContext).inflate(R.layout.layout_add_flash_card, null);
        edCardQuestion = view.findViewById(R.id.etCardQuestion);
        edCardAnswer = view.findViewById(R.id.etCardAnswer);
        fabSaveCard = view.findViewById(R.id.fabSaveCard);

        fabSaveCard.setOnClickListener(v -> {
            final String cardQuestion = edCardQuestion.getText().toString().trim();
            final String cardAnswer = edCardAnswer.getText().toString().trim();
            if (onSaveClickListener != null) {
                FlashCardModel card = new FlashCardModel(cardQuestion, cardAnswer);
                card.setBackgroundColor(RandomColorGenerator.getRandomColor());
                if (noteId != Integer.MIN_VALUE) {
                    card.setId(noteId);
                }
                onSaveClickListener.onSave(card);
            }
        });

        return view;
    }


    public void setOnSaveClickListener(OnSaveClickListener onSaveClickListener) {
        this.onSaveClickListener = onSaveClickListener;
    }
}

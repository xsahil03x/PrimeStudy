package com.magarex.flashcard.listener;

/**
 * Created by sahil on 7/1/19.
 **/
public interface OnArchiveClickListener {
    void onArchiveCard(int id);
}

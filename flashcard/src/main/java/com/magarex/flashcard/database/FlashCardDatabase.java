/*
 * Copyright 2018 DevCompat
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.magarex.flashcard.database;

import android.content.Context;

import com.magarex.flashcard.model.FlashCardModel;
import com.magarex.flashcard.utils.StatusConverter;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;


@Database(entities = {FlashCardModel.class}, version = 1, exportSchema = false)
@TypeConverters({StatusConverter.class})
public abstract class FlashCardDatabase extends RoomDatabase {

    private static final Object LOCK = new Object();
    private static final String DATABASE_NAME = "FlashCardDatabase";
    private static FlashCardDatabase sInstance;

    public static FlashCardDatabase getInstance(Context context) {
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = Room.databaseBuilder(context.getApplicationContext(),
                        FlashCardDatabase.class, FlashCardDatabase.DATABASE_NAME)
                        .fallbackToDestructiveMigration().build();
            }
        }
        return sInstance;
    }

    public abstract FlashCardDao flashCardDao();
}

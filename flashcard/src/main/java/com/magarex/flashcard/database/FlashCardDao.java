/*
 * Copyright 2018 DevCompat
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.magarex.flashcard.database;


import com.magarex.flashcard.model.FlashCardModel;
import com.magarex.flashcard.utils.Status;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import io.reactivex.Flowable;

@Dao
public interface FlashCardDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCard(FlashCardModel card);

    @Query("SELECT * FROM FlashCardModel")
    Flowable<List<FlashCardModel>> loadCards();

    @Query("SELECT * FROM FlashCardModel WHERE id = :id")
    Flowable<FlashCardModel> loadCardById(int id);

    @Query("SELECT * FROM FlashCardModel WHERE status = :status")
    Flowable<List<FlashCardModel>> loadNotesByStatus(Status status);

    @Query("DELETE FROM FlashCardModel WHERE id = :id")
    void deleteCard(int id);

    @Query("UPDATE FlashCardModel SET status = :status WHERE id = :id")
    void updateStatus(Status status, int id);
}

package com.magarex.flashcard.utils;

/**
 * Created by sahil on 5/1/19.
 **/

public enum Status {
    ACTIVE(0),
    ARCHIVE(1),
    TRASH(2);

    private int code;

    Status(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}

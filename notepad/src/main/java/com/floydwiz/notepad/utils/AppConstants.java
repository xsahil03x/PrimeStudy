package com.floydwiz.notepad.utils;

/**
 * Created by sahil on 14/1/19.
 **/
public class AppConstants {
    static final String NOTIFICATION_CHANNEL_ID = "reminder_notification_channel";
    static final CharSequence NOTIFICATION_CHANNEL_NAME = "Reminder Notifications";
    public static final String NOTE_KEY = "reminder_note";
    static final String NOTIFICATION_BODY = "notification_body";
    public static final String NOTE_ID = "reminder_id";
    public static final String NOTE_REMINDER = "reminder_time";
    static final String REMINDER_TYPE = "reminder_type";
    static final String ERROR_VALUE = "Error";
}
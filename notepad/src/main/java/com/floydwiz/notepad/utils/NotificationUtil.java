package com.floydwiz.notepad.utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;

import com.floydwiz.notepad.R;
import com.floydwiz.notepad.model.Note;
import com.floydwiz.notepad.model.Todo;

import java.util.Date;

import androidx.core.app.NotificationCompat;

/**
 * Created by sahil on 14/1/19.
 **/
class NotificationUtil {

    static <T> void showReminderNotification(final Context context, T type) {
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= 26) {
            NotificationChannel notificationChannel = new NotificationChannel(AppConstants.NOTIFICATION_CHANNEL_ID,
                    AppConstants.NOTIFICATION_CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_HIGH);
            if (manager != null) {
                manager.createNotificationChannel(notificationChannel);
            }
        }
        final NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, AppConstants.NOTIFICATION_CHANNEL_ID)
                .setAutoCancel(true)
                .setContentTitle(type instanceof Todo ? ((Todo) type).getTask() : ((Note) type).getTitle())
                .setContentText(type instanceof Todo ? "" : ((Note) type).getBody())
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setSmallIcon(R.drawable.ic_add_black_24dp)
                .setColor(Color.rgb(0, 100, 0))
                .setDefaults(NotificationCompat.DEFAULT_VIBRATE);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            notificationBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);
        }
        if (manager != null) {
            manager.notify(type instanceof Todo ? ((Todo) type).getId() : ((Note) type).getId(), notificationBuilder.build());
        }
    }
}

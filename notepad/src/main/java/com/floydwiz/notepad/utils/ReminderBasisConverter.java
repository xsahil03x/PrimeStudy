package com.floydwiz.notepad.utils;

import androidx.room.TypeConverter;

import static com.floydwiz.notepad.utils.ReminderBasis.DAILY;
import static com.floydwiz.notepad.utils.ReminderBasis.ONCE;
import static com.floydwiz.notepad.utils.ReminderBasis.WEEKLY;

/**
 * Created by sahil on 16/1/19.
 **/
public class ReminderBasisConverter {
    @TypeConverter
    public static ReminderBasis toReminderBasis(int basis) {
        if (basis == ONCE.getCode()) {
            return ONCE;
        } else if (basis == DAILY.getCode()) {
            return DAILY;
        } else if (basis == WEEKLY.getCode()) {
            return WEEKLY;
        } else {
            throw new IllegalArgumentException("Could not recognize basis");
        }
    }

    @TypeConverter
    public static int toInt(ReminderBasis basis) {
        return basis.getCode();
    }
}

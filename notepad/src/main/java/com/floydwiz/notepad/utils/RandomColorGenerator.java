package com.floydwiz.notepad.utils;

import android.graphics.Color;

import java.util.Random;

/**
 * Created by sahil on 11/1/19.
 **/
public class RandomColorGenerator {
    public static int getRandomColor() {
        Random random = new Random();
        int a = random.nextInt(colors.length);
        return colors[a];
    }

    private static int colors[] = {
            Color.parseColor("#ffcdd2"),
            Color.parseColor("#f8bbd0"),
            Color.parseColor("#e1bee7"),
            Color.parseColor("#d1c4e9"),
            Color.parseColor("#c5cae9"),
            Color.parseColor("#bbdefb"),
            Color.parseColor("#b3e5fc"),
            Color.parseColor("#b2ebf2"),
            Color.parseColor("#b2dfdb"),
            Color.parseColor("#c8e6c9"),
            Color.parseColor("#dcedc8"),
            Color.parseColor("#f0f4c3"),
            Color.parseColor("#fff9c4"),
            Color.parseColor("#ffecb3"),
            Color.parseColor("#ffe0b2"),
            Color.parseColor("#ffccbc"),
            Color.parseColor("#d7ccc8"),
            Color.parseColor("#f5f5f5"),
            Color.parseColor("#cfd8dc"),
    };
}

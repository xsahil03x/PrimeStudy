package com.floydwiz.notepad.utils;

import androidx.room.TypeConverter;

import static com.floydwiz.notepad.utils.Status.ACTIVE;
import static com.floydwiz.notepad.utils.Status.ARCHIVE;
import static com.floydwiz.notepad.utils.Status.TRASH;

/**
 * Created by sahil on 5/1/19.
 **/
public class StatusConverter {

    @TypeConverter
    public static Status toStatus(int status) {
        if (status == ACTIVE.getCode()) {
            return ACTIVE;
        } else if (status == ARCHIVE.getCode()) {
            return ARCHIVE;
        } else if (status == TRASH.getCode()) {
            return TRASH;
        } else {
            throw new IllegalArgumentException("Could not recognize status");
        }
    }

    @TypeConverter
    public static int toInt(Status status) {
        return status.getCode();
    }
}
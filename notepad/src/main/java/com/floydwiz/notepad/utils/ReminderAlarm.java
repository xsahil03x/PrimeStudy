package com.floydwiz.notepad.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.floydwiz.notepad.model.Note;
import com.floydwiz.notepad.model.Todo;

import static com.floydwiz.notepad.utils.AlarmManagerUtil.deserializeFromJson;
import static com.floydwiz.notepad.utils.AppConstants.NOTIFICATION_BODY;
import static com.floydwiz.notepad.utils.AppConstants.REMINDER_TYPE;

public class ReminderAlarm extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getExtras().getString(REMINDER_TYPE).equals("Todo")) {
            Todo todo = deserializeFromJson(intent.getExtras().getString(NOTIFICATION_BODY), Todo.class);
            NotificationUtil.showReminderNotification(context, todo);
        } else {
            Note note = deserializeFromJson(intent.getExtras().getString(NOTIFICATION_BODY), Note.class);
            NotificationUtil.showReminderNotification(context, note);
        }
    }
}
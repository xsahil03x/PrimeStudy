package com.floydwiz.notepad.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.floydwiz.notepad.model.Note;
import com.floydwiz.notepad.model.Todo;
import com.google.gson.Gson;

import static com.floydwiz.notepad.utils.AppConstants.NOTIFICATION_BODY;
import static com.floydwiz.notepad.utils.AppConstants.REMINDER_TYPE;

public class AlarmManagerUtil {

    private static <T> String serializeToJson(T type) {
        Gson gson = new Gson();
        return gson.toJson(type);
    }

    static <T> T deserializeFromJson(String jsonString, Class<T> classOfT) {
        Gson gson = new Gson();
        return gson.fromJson(jsonString, classOfT);
    }

    public static <T> void scheduleReminderWork(Context context, T type, String reminderType) {
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, ReminderAlarm.class);
        i.putExtra(NOTIFICATION_BODY, serializeToJson(type));
        i.putExtra(REMINDER_TYPE, reminderType);
        PendingIntent pi = PendingIntent.getBroadcast(context,
                type instanceof Todo ? ((Todo) type).getId() : ((Note) type).getId(),
                i, PendingIntent.FLAG_UPDATE_CURRENT);
        switch (type instanceof Todo ? ((Todo) type).getReminderBasis() : ((Note) type).getReminderBasis()) {
            case ONCE:
                manager.set(
                        AlarmManager.RTC_WAKEUP,
                        type instanceof Todo ? ((Todo) type).getReminderTime() : ((Note) type).getReminderTime(),
                        pi);
                break;
            case DAILY:
                manager.setRepeating(
                        AlarmManager.RTC_WAKEUP,
                        type instanceof Todo ? ((Todo) type).getReminderTime() : ((Note) type).getReminderTime(),
                        AlarmManager.INTERVAL_DAY,
                        pi);
                break;
            case WEEKLY:
                manager.setRepeating(
                        AlarmManager.RTC_WAKEUP,
                        type instanceof Todo ? ((Todo) type).getReminderTime() : ((Note) type).getReminderTime(),
                        AlarmManager.INTERVAL_DAY * 7,
                        pi);
                break;
        }
    }

    public static void cancelReminderWork(Context context, int id) {
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, ReminderAlarm.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, id, i, PendingIntent.FLAG_UPDATE_CURRENT);
        manager.cancel(pi);
    }
}

package com.floydwiz.notepad.utils;

/**
 * Created by sahil on 16/1/19.
 **/
public enum ReminderBasis {
    ONCE(0),
    DAILY(1),
    WEEKLY(2);

    private int code;

    ReminderBasis(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}

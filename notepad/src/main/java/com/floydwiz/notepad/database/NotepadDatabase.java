/*
 * Copyright 2018 DevCompat
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.floydwiz.notepad.database;

import android.content.Context;

import com.floydwiz.notepad.model.Note;
import com.floydwiz.notepad.model.Todo;
import com.floydwiz.notepad.utils.ReminderBasisConverter;
import com.floydwiz.notepad.utils.StatusConverter;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;


@Database(entities = {Note.class, Todo.class}, version = 2, exportSchema = false)
@TypeConverters({StatusConverter.class, ReminderBasisConverter.class})
public abstract class NotepadDatabase extends RoomDatabase {

    private static final Object LOCK = new Object();
    private static final String DATABASE_NAME = "NotepadDatabase";
    private static NotepadDatabase sInstance;

    public static NotepadDatabase getInstance(Context context) {
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = Room.databaseBuilder(context.getApplicationContext(),
                        NotepadDatabase.class,
                        NotepadDatabase.DATABASE_NAME).fallbackToDestructiveMigration()
                        .build();
            }
        }
        return sInstance;
    }

    public abstract NotepadDao notepadDao();

    public abstract TodoDao todoDao();
}

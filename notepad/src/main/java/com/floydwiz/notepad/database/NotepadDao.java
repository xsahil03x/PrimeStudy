/*
 * Copyright 2018 DevCompat
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.floydwiz.notepad.database;


import com.floydwiz.notepad.model.Note;
import com.floydwiz.notepad.model.Todo;
import com.floydwiz.notepad.utils.Status;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import io.reactivex.Flowable;

@Dao
public interface NotepadDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertNote(Note note);

    @Query("SELECT * FROM Notes")
    Flowable<List<Note>> loadNotes();

    @Query("SELECT * FROM Notes WHERE status = :status")
    Flowable<List<Note>> loadNotesByStatus(Status status);

    @Query("SELECT * FROM Notes WHERE id = :id")
    Flowable<Note> loadNoteById(int id);

    @Query("DELETE FROM Notes WHERE id = :id")
    void deleteNote(int id);

    @Query("UPDATE Notes SET status = :status WHERE id = :id")
    void updateStatus(Status status, int id);

}

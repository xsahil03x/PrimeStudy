package com.floydwiz.notepad.database;

import com.floydwiz.notepad.model.Todo;
import com.floydwiz.notepad.utils.ReminderBasis;
import com.floydwiz.notepad.utils.Status;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import io.reactivex.Flowable;

@Dao
public interface TodoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertTodo(Todo todo);

    @Query("SELECT * FROM ToDos")
    Flowable<List<Todo>> loadTodos();

    @Query("SELECT * FROM ToDos WHERE id = :id")
    Flowable<Todo> loadTodoById(int id);

    @Query("DELETE FROM ToDos WHERE id = :id")
    void deleteTodo(int id);

    @Query("UPDATE ToDos SET isChecked = :checked WHERE id = :id")
    void updateChecked(Boolean checked, int id);

    @Query("UPDATE ToDos SET isReminderSet = :isReminderSet," +
            "reminderTime = :remainingTimeToRemind," +
            " reminderBasis = :reminderBasis" +
            " WHERE id = :id")
    void updateReminder(Boolean isReminderSet, long remainingTimeToRemind,
                        ReminderBasis reminderBasis, int id);

}

package com.floydwiz.notepad.ui;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.floydwiz.notepad.R;
import com.floydwiz.notepad.adapter.TodoAdapter;
import com.floydwiz.notepad.database.NotepadDatabase;
import com.floydwiz.notepad.listener.OnDeleteClickListener;
import com.floydwiz.notepad.listener.OnReminderClickListener;
import com.floydwiz.notepad.listener.OnTodoClickListener;
import com.floydwiz.notepad.listener.OnTodoReminderClickListener;
import com.floydwiz.notepad.model.Todo;
import com.floydwiz.notepad.utils.GridSpacingItemDecoration;
import com.floydwiz.notepad.utils.ReminderBasis;
import com.floydwiz.shared.AppUtils;
import com.google.android.material.snackbar.Snackbar;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;

import static com.floydwiz.notepad.utils.AlarmManagerUtil.cancelReminderWork;
import static com.floydwiz.notepad.utils.AlarmManagerUtil.scheduleReminderWork;

public class TodoView implements OnTodoClickListener, OnReminderClickListener, OnDeleteClickListener {

    private Context mContext;
    private View view;
    private RecyclerView rvTodos;
    private EditText etTodo;
    private TodoAdapter todoAdapter;
    private TextView tvEmptyList;
    private ImageView ivEmptyList;
    private ImageButton btnAddTodo;
    private NotepadDatabase mDatabase;
    private CompositeDisposable mDisposable;
    private OnTodoReminderClickListener onTodoReminderClickListener;

    private static final String TAG = "TodoView";

    TodoView(Context context) {
        mContext = context;
        mDatabase = NotepadDatabase.getInstance(mContext);
        mDisposable = new CompositeDisposable();
    }

    public View getView() {
        view = LayoutInflater.from(mContext).inflate(R.layout.layout_todo, null);
        rvTodos = view.findViewById(R.id.rvTodos);
        btnAddTodo = view.findViewById(R.id.btnAddTodo);
        etTodo = view.findViewById(R.id.etTodo);
        tvEmptyList = view.findViewById(R.id.tvEmptyList);
        ivEmptyList = view.findViewById(R.id.ivEmptyList);
        rvTodos.setLayoutManager(new LinearLayoutManager(mContext));
        rvTodos.addItemDecoration(new GridSpacingItemDecoration(1, AppUtils.dpToPx(4), true));
        todoAdapter = new TodoAdapter(mContext, this, this, this);
        rvTodos.setAdapter(todoAdapter);
        loadTodo();

        btnAddTodo.setOnClickListener(v -> {
            String task = etTodo.getText().toString().trim();
            if (TextUtils.isEmpty(etTodo.getText()))
                Toast.makeText(mContext, "Please fill in some details", Toast.LENGTH_SHORT).show();
            else {
                mDisposable.add(Completable.fromAction(() -> mDatabase.todoDao().insertTodo(new Todo(task)))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(() -> {
                            if (tvEmptyList.getVisibility() == View.VISIBLE) {
                                tvEmptyList.setVisibility(View.GONE);
                                ivEmptyList.setVisibility(View.GONE);
                                rvTodos.setVisibility(View.VISIBLE);
                            }
                            Toast.makeText(mContext, "Todo Saved Successfully", Toast.LENGTH_SHORT).show();
                        }));
            }
        });

        return view;
    }

    void setOnTodoReminderClickListener(OnTodoReminderClickListener onTodoReminderClickListener) {
        this.onTodoReminderClickListener = onTodoReminderClickListener;
    }

    private void loadTodo() {
        mDisposable.add(mDatabase.todoDao().loadTodos()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(todoList -> {
                    if (todoList.size() > 0)
                        todoAdapter.addTodoToList(todoList);
                    else {
                        rvTodos.setVisibility(View.GONE);
                        tvEmptyList.setVisibility(View.VISIBLE);
                        ivEmptyList.setVisibility(View.VISIBLE);
                    }
                }));
    }

    @Override
    public void onDeleteNote(int id) {
        mDisposable.add(Completable.fromAction(() -> mDatabase.todoDao().deleteTodo(id))
                .subscribeOn(Schedulers.io())
                .subscribe(() -> {
                    cancelReminderWork(mContext, id);
                    AppUtils.showMaterialSnackBar(view, "Deleted", Snackbar.LENGTH_SHORT);
                }));
    }

    @Override
    public void onReminderClick(int id) {
        if (onTodoReminderClickListener != null)
            onTodoReminderClickListener.onReminderClick(id);
    }

    @Override
    public void onItemClick(Boolean checked, int id) {
        mDisposable.add(Completable.fromAction(() -> mDatabase.todoDao().updateChecked(!checked, id))
                .subscribeOn(Schedulers.io())
                .subscribe(() -> AppUtils.showBasicSnackBar(view, "Updated", Snackbar.LENGTH_SHORT)));
    }

    void setTodoReminder(long time, String basis, int id) {
        ReminderBasis reminderBasis = ReminderBasis.ONCE;
        switch (basis) {
            case "Once":
                reminderBasis = ReminderBasis.ONCE;
                break;
            case "Daily":
                reminderBasis = ReminderBasis.DAILY;
                break;
            case "Weekly":
                reminderBasis = ReminderBasis.WEEKLY;
                break;
        }
        ReminderBasis finalReminderBasis = reminderBasis;
        mDisposable.add(Completable.fromAction(() -> mDatabase.todoDao().updateReminder(
                true, time, finalReminderBasis, id
        ))
                .subscribeOn(Schedulers.io())
                .subscribe(() -> {
                    setTodoReminder(id);
                    AppUtils.showBasicSnackBar(view, "Reminder Added", Snackbar.LENGTH_SHORT);
                }));
    }

    private void setTodoReminder(int id) {
        mDisposable.add(mDatabase.todoDao().loadTodoById(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSubscriber<Todo>() {
                    @Override
                    public void onNext(Todo todo) {
                        scheduleReminderWork(mContext, todo, "Todo");
                    }

                    @Override
                    public void onError(Throwable t) {
                        Log.d(TAG, "onError() called with: t = [" + t + "]");
                    }

                    @Override
                    public void onComplete() {

                    }
                }));
    }

    void enableView(Boolean b) {
        etTodo.setEnabled(b);
        rvTodos.setEnabled(b);
        btnAddTodo.setEnabled(b);
    }
}

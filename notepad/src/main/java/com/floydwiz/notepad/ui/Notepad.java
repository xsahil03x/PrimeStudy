/*
 * Copyright 2018 DevCompat
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.floydwiz.notepad.ui;

import android.content.Context;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.floydwiz.notepad.R;
import com.floydwiz.notepad.database.NotepadDatabase;
import com.floydwiz.notepad.listener.OnAddClickListener;
import com.floydwiz.notepad.listener.OnCancelClickListener;
import com.floydwiz.notepad.listener.OnOKClickListener;
import com.floydwiz.notepad.listener.OnSaveClickListener;
import com.floydwiz.notepad.listener.OnTodoReminderClickListener;
import com.floydwiz.notepad.model.Note;
import com.floydwiz.notepad.utils.AlarmManagerUtil;
import com.floydwiz.notepad.utils.SearchViewFormatter;
import com.google.android.material.navigation.NavigationView;
import com.jakewharton.rxbinding3.appcompat.RxSearchView;

import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DefaultObserver;
import io.reactivex.schedulers.Schedulers;

public class Notepad implements OnAddClickListener, OnSaveClickListener, OnCancelClickListener,
        OnOKClickListener, OnTodoReminderClickListener, NavigationView.OnNavigationItemSelectedListener {

    private Context mContext;
    private AddNoteView addNoteView;
    private ShowNoteView showNoteView;
    private ArchiveView archiveView;
    private TrashView trashView;
    private TodoView todoView;
    private CustomReminderDialog reminderDialog;
    private TextView tvToolbarTitle;
    private MenuItem searchItem, paletteItem, reminderItem;
    private ImageView ivDrawerMenu, ivBack;
    private FrameLayout frameNotepadView;
    private NotepadDatabase mDatabase;
    private DrawerLayout mDrawerLayout;
    private CompositeDisposable mDisposable;

    private static final String TAG = "Notepad";

    public Notepad(Context context) {
        mContext = context;
        mDisposable = new CompositeDisposable();
    }

    public View getView() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_notepad, null);
        frameNotepadView = view.findViewById(R.id.frameNotepadView);
        mDatabase = NotepadDatabase.getInstance(mContext);
        ivDrawerMenu = view.findViewById(R.id.ivDrawerMenu);
        ivBack = view.findViewById(R.id.ivBack);
        NavigationView navigationView = view.findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        Toolbar mainToolbar = view.findViewById(R.id.mainToolbar);
        mainToolbar.inflateMenu(R.menu.toolbar_menu_items);
        handleMenuItemClicks(mainToolbar.getMenu());
        tvToolbarTitle = view.findViewById(R.id.toolbar_title);
        mDrawerLayout = view.findViewById(R.id.drawerLayout);
        addNoteView = new AddNoteView(mContext);
        showNoteView = new ShowNoteView(mContext);
        archiveView = new ArchiveView(mContext);
        trashView = new TrashView(mContext);
        todoView = new TodoView(mContext);
        reminderDialog = new CustomReminderDialog(mContext);
        addNoteView.setOnSaveClickListener(this);
        showNoteView.setOnAddClickListener(this);
        reminderDialog.setOnCancelClickListener(this);
        reminderDialog.setOnOKClickListener(this);
        todoView.setOnTodoReminderClickListener(this);

        ivDrawerMenu.setOnClickListener(v -> mDrawerLayout.openDrawer(GravityCompat.START));
        frameNotepadView.addView(showNoteView.getView());

        ivBack.setOnClickListener(v -> {
            frameNotepadView.removeAllViews();
            frameNotepadView.addView(showNoteView.getView());
            changeToolbar(false);
        });

        return view;
    }

    private void handleMenuItemClicks(Menu menu) {
        searchItem = menu.findItem(R.id.btnSearch);
        SearchView searchview = (SearchView) searchItem.getActionView();

        new SearchViewFormatter()
                .setSearchIconResource(R.drawable.ic_search_black_24dp, true, false)
                .setSearchCloseIconResource(R.drawable.ic_clear_white_24dp)
                .setSearchTextColorResource(R.color.white_yellow)
                .setSearchHintText("Enter keywords")
                .setSearchHintColorResource(R.color.grey)
                .setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS)
                .format(searchview);

        RxSearchView.queryTextChanges(searchview)
                .skipInitialValue()
                .debounce(200, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DefaultObserver<CharSequence>() {
                    @Override
                    public void onNext(CharSequence charSequence) {
                        if (showNoteView != null) {
                            showNoteView.onTextChange(charSequence.toString());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "onError() called with: e = [" + e + "]");
                    }

                    @Override
                    public void onComplete() {
                    }
                });


//        MenuItem gridItem = menu.findItem(R.id.btnGrid);
//        gridItem.setOnMenuItemClickListener(item -> {
//            showNoteView.setGridView();
//            return false;
//        });

        paletteItem = menu.findItem(R.id.btnPallete);
        paletteItem.setOnMenuItemClickListener(item -> {
            addNoteView.setRandomBackground();
            return false;
        });

        reminderItem = menu.findItem(R.id.btnReminder);
        reminderItem.setOnMenuItemClickListener(item -> {
            addNoteView.enableView(false);
            showReminderDialog(Integer.MIN_VALUE);
            return false;
        });
    }

    private void showReminderDialog(int id) {
        frameNotepadView.addView(reminderDialog.getView(id));
    }

    @Override
    public void onAdd(int backgroundColor, String title, String body, int id) {
        frameNotepadView.removeAllViews();
        showNoteView.clearAll();
//        frameNotepadView.addView(todoView.getView());

        frameNotepadView.addView(addNoteView.getView(new Note(backgroundColor, id, title, body)));
        Log.d(TAG, "onAdd() called with: backgroundColor = [" + backgroundColor + "], title = [" + title + "], body = [" + body + "], id = [" + id + "]");
        changeToolbar(true);
    }

    @Override
    public void onSave(Note note) {
        if (note.getTitle().isEmpty() || note.getBody().isEmpty()) {
            Toast.makeText(mContext, "Please fill in some details", Toast.LENGTH_SHORT).show();
        } else {
            mDisposable.add(Completable.fromAction(() -> mDatabase.notepadDao().insertNote(note))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(() -> {
                        Toast.makeText(mContext, "Note Saved Successfully", Toast.LENGTH_SHORT).show();
                        showNoteView.clearAll();
                        frameNotepadView.removeAllViews();
                        frameNotepadView.addView(showNoteView.getView());
                        changeToolbar(false);
                    }));
            if (note.getReminderSet())
                setNoteReminder(note);
        }
    }

    private void setNoteReminder(Note note) {
        AlarmManagerUtil.scheduleReminderWork(mContext, note, "Note");
        Log.d(TAG, "onSave: Work successfully scheduled" + note.getId());
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int i = item.getItemId();
        if (i == R.id.menu_notes) {
            frameNotepadView.removeAllViews();
            frameNotepadView.addView(showNoteView.getView());
            searchItem.setVisible(true);
            tvToolbarTitle.setText("Notepad");
        } else if (i == R.id.menu_todo) {
            frameNotepadView.removeAllViews();
            frameNotepadView.addView(todoView.getView());
            searchItem.setVisible(false);
            tvToolbarTitle.setText("Todo's");
        } else if (i == R.id.menu_archive) {
            frameNotepadView.removeAllViews();
            frameNotepadView.addView(archiveView.getView());
            searchItem.setVisible(false);
            tvToolbarTitle.setText("Archives");
        } else if (i == R.id.menu_trash) {
            frameNotepadView.removeAllViews();
            frameNotepadView.addView(trashView.getView());
            searchItem.setVisible(false);
            tvToolbarTitle.setText("Trash");
        }

        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void changeToolbar(Boolean shouldChange) {
        if (shouldChange) {
            searchItem.setVisible(false);
            paletteItem.setVisible(true);
            reminderItem.setVisible(true);
            tvToolbarTitle.setGravity(Gravity.START);
            ivDrawerMenu.setVisibility(View.GONE);
            ivBack.setVisibility(View.VISIBLE);
        } else {
            searchItem.setVisible(true);
            paletteItem.setVisible(false);
            reminderItem.setVisible(false);
            tvToolbarTitle.setGravity(Gravity.CENTER);
            ivDrawerMenu.setVisibility(View.VISIBLE);
            ivBack.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCancelClicked() {
        frameNotepadView.removeViewAt(1);
        if (tvToolbarTitle.getText().toString().equals("Notepad")) {
            addNoteView.enableView(true);
        } else {
            todoView.enableView(true);
        }
    }

    @Override
    public void onOKClicked(long time, String basis, int id) {
        frameNotepadView.removeViewAt(1);
        if (tvToolbarTitle.getText().toString().equals("Notepad")) {
            addNoteView.enableView(true);
            addNoteView.setReminder(time, basis);
        } else {
            todoView.setTodoReminder(time, basis, id);
            todoView.enableView(false);
        }
    }

    @Override
    public void onReminderClick(int id) {
        showReminderDialog(id);
        todoView.enableView(false);
    }
}

/*
 * Copyright 2018 DevCompat
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.floydwiz.notepad.ui;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.floydwiz.notepad.R;
import com.floydwiz.notepad.adapter.NotepadFilterableAdapter;
import com.floydwiz.notepad.database.NotepadDatabase;
import com.floydwiz.notepad.listener.OnAddClickListener;
import com.floydwiz.notepad.listener.OnArchiveClickListener;
import com.floydwiz.notepad.listener.OnDeleteClickListener;
import com.floydwiz.notepad.listener.OnItemClickListener;
import com.floydwiz.notepad.utils.GridSpacingItemDecoration;
import com.floydwiz.notepad.utils.Status;
import com.floydwiz.shared.AppUtils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.floydwiz.notepad.utils.AlarmManagerUtil.cancelReminderWork;

public class ShowNoteView implements OnDeleteClickListener, OnItemClickListener, OnArchiveClickListener {
    private Context mContext;
    private View view;
    private RecyclerView rvNotes;
    private NotepadFilterableAdapter mAdapter;
    private NotepadDatabase mDatabase;
    private TextView tvEmptyList;
    private ImageView ivEmptyList;
    private GridLayoutManager layoutManager;
    private CompositeDisposable mDisposable;

    private static final String TAG = "ShowNotepad";
    private OnAddClickListener onAddClickListener;

    ShowNoteView(Context context) {
        mContext = context;
        mDisposable = new CompositeDisposable();
        mDatabase = NotepadDatabase.getInstance(mContext);
    }

    public View getView() {
        view = LayoutInflater.from(mContext).inflate(R.layout.layout_show_note, null);
        FloatingActionButton fabAddNote = view.findViewById(R.id.fabAddNote);
        tvEmptyList = view.findViewById(R.id.tvEmptyList);
        ivEmptyList = view.findViewById(R.id.ivEmptyList);
        rvNotes = view.findViewById(R.id.rvNotes);
        layoutManager = new GridLayoutManager(mContext, 1);
        rvNotes.setLayoutManager(layoutManager);
        rvNotes.addItemDecoration(new GridSpacingItemDecoration(1, AppUtils.dpToPx(4), true));
        mAdapter = new NotepadFilterableAdapter(mContext, this, this, this, Status.ACTIVE);
        rvNotes.setAdapter(mAdapter);
        loadNotes();
        fabAddNote.setOnClickListener(v -> {
            if (onAddClickListener != null) {
                onAddClickListener.onAdd(Color.WHITE, "", "", Integer.MIN_VALUE);
            }
        });

        return view;
    }

    private void loadNotes() {
        mDisposable.add(mDatabase.notepadDao().loadNotesByStatus(Status.ACTIVE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(notes -> {
                    if (notes.size() > 0)
                        mAdapter.addNotesToList(notes);
                    else {
                        rvNotes.setVisibility(View.GONE);
                        tvEmptyList.setVisibility(View.VISIBLE);
                        ivEmptyList.setVisibility(View.VISIBLE);
                    }
                }));
    }

    void setOnAddClickListener(OnAddClickListener onAddClickListener) {
        this.onAddClickListener = onAddClickListener;
    }

    @Override
    public void onDeleteNote(int id) {
        mDisposable.add(Completable.fromAction(() -> mDatabase.notepadDao().updateStatus(Status.TRASH, id))
                .subscribeOn(Schedulers.io())
                .subscribe(() -> {
                    AppUtils.showBasicSnackBar(view, "Stored in trash", Snackbar.LENGTH_SHORT);
                    cancelReminderWork(mContext, id);
                    Log.d(TAG, "onDeleteNote() called with: id = [" + id + "]");
                }));
    }

    @Override
    public void onItemClick(int id) {
        mDisposable.add(mDatabase.notepadDao().loadNoteById(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(note -> onAddClickListener.onAdd(note.getBackgroundColor(), note.getTitle(), note.getBody(), id)));
    }

    @Override
    public void onArchiveNote(int id) {
        mDisposable.add(Completable.fromAction(() -> mDatabase.notepadDao().updateStatus(Status.ARCHIVE, id))
                .subscribeOn(Schedulers.io())
                .subscribe(() -> AppUtils.showBasicSnackBar(view, "Stored in archive", Snackbar.LENGTH_SHORT)));
    }

    void onTextChange(String text) {
        mAdapter.getFilter().filter(text);
    }

//    void setGridView() {
//        if (layoutManager.getSpanCount() == 1)
//            layoutManager.setSpanCount(2);
//        else layoutManager.setSpanCount(1);
//        mAdapter.notifyDataSetChanged();
//    }

    void clearAll() {
        mDisposable.clear();
    }
}

package com.floydwiz.notepad.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.floydwiz.notepad.R;
import com.floydwiz.notepad.adapter.NotepadFilterableAdapter;
import com.floydwiz.notepad.database.NotepadDatabase;
import com.floydwiz.notepad.listener.OnArchiveClickListener;
import com.floydwiz.notepad.listener.OnDeleteClickListener;
import com.floydwiz.notepad.listener.OnItemClickListener;
import com.floydwiz.notepad.utils.GridSpacingItemDecoration;
import com.floydwiz.notepad.utils.Status;
import com.floydwiz.shared.AppUtils;
import com.google.android.material.snackbar.Snackbar;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.Completable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by sahil on 4/1/19.
 **/
public class ArchiveView implements OnItemClickListener, OnDeleteClickListener, OnArchiveClickListener {
    private Context mContext;
    private NotepadDatabase mDatabase;
    private TextView tvEmptyList;
    private View view;
    private ImageView ivEmptyList;
    private RecyclerView rvArchives;
    private NotepadFilterableAdapter mAdapter;
    private CompositeDisposable mDisposable;

    ArchiveView(Context context) {
        mContext = context;
        mDisposable = new CompositeDisposable();
        mDatabase = NotepadDatabase.getInstance(mContext);
    }

    public View getView() {
        view = LayoutInflater.from(mContext).inflate(R.layout.layout_archive, null);
        tvEmptyList = view.findViewById(R.id.tvEmptyList);
        ivEmptyList = view.findViewById(R.id.ivEmptyList);
        rvArchives = view.findViewById(R.id.rvArchives);
        rvArchives.setLayoutManager(new LinearLayoutManager(mContext));
        rvArchives.addItemDecoration(new GridSpacingItemDecoration(1, AppUtils.dpToPx(4), true));
        mAdapter = new NotepadFilterableAdapter(mContext, this, this, this, Status.ARCHIVE);
        rvArchives.setAdapter(mAdapter);
        loadNotes();
        return view;
    }

    private void loadNotes() {
        mDisposable.add(mDatabase.notepadDao().loadNotesByStatus(Status.ARCHIVE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(notes -> {
                    if (notes.size() > 0)
                        mAdapter.addNotesToList(notes);
                    else {
                        rvArchives.setVisibility(View.GONE);
                        tvEmptyList.setVisibility(View.VISIBLE);
                        ivEmptyList.setVisibility(View.VISIBLE);
                    }
                }));
    }

    @Override
    public void onDeleteNote(int id) {
        mDisposable.add(Completable.fromAction(() -> mDatabase.notepadDao().updateStatus(Status.TRASH, id))
                .subscribeOn(Schedulers.io())
                .subscribe(() -> AppUtils.showBasicSnackBar(view, "Stored in trash", Snackbar.LENGTH_SHORT)));
    }

    @Override
    public void onItemClick(int id) {
        // TODO : Do something awesome
    }

    @Override
    public void onArchiveNote(int id) {
        // This will unarchive this note
        mDisposable.add(Completable.fromAction(() -> mDatabase.notepadDao().updateStatus(Status.ACTIVE, id))
                .subscribeOn(Schedulers.io())
                .subscribe(() -> AppUtils.showMaterialSnackBar(view, "Successfully Restored", Snackbar.LENGTH_SHORT)));
    }

    void clearAll() {
        mDisposable.clear();
    }
}

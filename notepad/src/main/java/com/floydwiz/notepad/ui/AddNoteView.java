/*
 * Copyright 2018 DevCompat
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.floydwiz.notepad.ui;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.floydwiz.notepad.R;
import com.floydwiz.notepad.listener.OnSaveClickListener;
import com.floydwiz.notepad.model.Note;
import com.floydwiz.notepad.utils.RandomColorGenerator;
import com.floydwiz.notepad.utils.ReminderBasis;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

class AddNoteView {
    private Context mContext;
    private View view;
    private EditText edNoteTitle, edNoteBody;
    private int noteId = Integer.MIN_VALUE;
    private int currentBackgroundColor = Color.WHITE;
    private Note noteToBeSaved;
    private FloatingActionButton fabSaveNote;
    private OnSaveClickListener onSaveClickListener;

    AddNoteView(Context context) {
        mContext = context;
    }

    View getView(Note oldNote) {
        view = LayoutInflater.from(mContext).inflate(R.layout.layout_add_note, null);
        edNoteTitle = view.findViewById(R.id.etNoteTitle);
        edNoteBody = view.findViewById(R.id.etNoteBody);
        fabSaveNote = view.findViewById(R.id.fabSaveNote);
        setSavedData(oldNote.getBackgroundColor(), oldNote.getTitle(), oldNote.getBody(), oldNote.getId());
        noteToBeSaved = new Note();
        fabSaveNote.setOnClickListener(v -> {
            final String noteTitle = edNoteTitle.getText().toString().trim();
            final String noteBody = edNoteBody.getText().toString().trim();
            final String noteDate = new SimpleDateFormat("dd'th' MMM yyyy", Locale.getDefault()).format(new Date());
            if (onSaveClickListener != null) {
//                noteToBeSaved.setId(UUID.randomUUID().toString());
                noteToBeSaved.setTitle(noteTitle);
                noteToBeSaved.setBody(noteBody);
                noteToBeSaved.setTime(noteDate);
                noteToBeSaved.setBackgroundColor(currentBackgroundColor);
                if (noteId != Integer.MIN_VALUE) {
                    noteToBeSaved.setId(noteId);
                }
                onSaveClickListener.onSave(noteToBeSaved);
            }
        });

        return view;
    }

    void setOnSaveClickListener(OnSaveClickListener onSaveClickListener) {
        this.onSaveClickListener = onSaveClickListener;
    }

    private void setSavedData(int backgroundColor, String title, String body, int noteId) {
        this.currentBackgroundColor = backgroundColor;
        view.setBackgroundColor(currentBackgroundColor);
        edNoteTitle.setText(title);
        edNoteBody.setText(body);
        this.noteId = noteId;
    }

    void setRandomBackground() {
        this.currentBackgroundColor = RandomColorGenerator.getRandomColor();
        view.setBackgroundColor(currentBackgroundColor);
    }

    void enableView(Boolean b) {
        edNoteTitle.setEnabled(b);
        edNoteBody.setEnabled(b);
        fabSaveNote.setEnabled(b);
    }

    void setReminder(long time, String basis) {
        if (noteToBeSaved != null) {
            noteToBeSaved.setReminderSet(true);
            switch (basis) {
                case "Once":
                    noteToBeSaved.setReminderBasis(ReminderBasis.ONCE);
                    break;
                case "Daily":
                    noteToBeSaved.setReminderBasis(ReminderBasis.DAILY);
                    break;
                case "Weekly":
                    noteToBeSaved.setReminderBasis(ReminderBasis.WEEKLY);
                    break;
                default:
                    noteToBeSaved.setReminderBasis(ReminderBasis.ONCE);
            }
            noteToBeSaved.setReminderTime(time);
        }
    }
}
package com.floydwiz.notepad.ui;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.floydwiz.notepad.R;
import com.floydwiz.notepad.listener.OnCancelClickListener;
import com.floydwiz.notepad.listener.OnOKClickListener;
import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;

/**
 * Created by sahil on 16/1/19.
 **/
public class CustomReminderDialog {
    private Context mContext;
    private View view;
    private Button btnCancel, btnOK;
    private Spinner spnBasis;
    private OnCancelClickListener onCancelClickListener;
    private OnOKClickListener onOKClickListener;

    CustomReminderDialog(Context context) {
        mContext = context;
    }

    public View getView(int id) {
        view = LayoutInflater.from(mContext).inflate(R.layout.custom_reminder_dialog, null);
        btnCancel = view.findViewById(R.id.btnCancel);
        btnOK = view.findViewById(R.id.btnOK);
        spnBasis = view.findViewById(R.id.spnBasis);
        SingleDateAndTimePicker picker = view.findViewById(R.id.dateTimePicker);
        picker.setIsAmPm(false);
        picker.setCyclic(false);
        btnCancel.setOnClickListener(v -> {
            if (onCancelClickListener != null) {
                onCancelClickListener.onCancelClicked();
            }
        });

        btnOK.setOnClickListener(v -> {
            String basis = spnBasis.getSelectedItem().toString().trim();
            long reminderTime = picker.getDate().getTime();
            if (reminderTime == 0) {
                Toast.makeText(mContext, "fuiafuia", Toast.LENGTH_SHORT).show();
            } else if (TextUtils.isEmpty(basis)) {
                Toast.makeText(mContext, "ahahahaah", Toast.LENGTH_SHORT).show();
            } else if (onOKClickListener != null) {
                onOKClickListener.onOKClicked(reminderTime, basis,id);
            }
        });

        return view;
    }

    public void setOnCancelClickListener(OnCancelClickListener onCancelClickListener) {
        this.onCancelClickListener = onCancelClickListener;
    }

    public void setOnOKClickListener(OnOKClickListener onOKClickListener) {
        this.onOKClickListener = onOKClickListener;
    }
}

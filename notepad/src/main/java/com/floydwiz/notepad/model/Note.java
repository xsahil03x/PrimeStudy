/*
 * Copyright 2018 DevCompat
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.floydwiz.notepad.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.floydwiz.notepad.utils.ReminderBasis;
import com.floydwiz.notepad.utils.ReminderBasisConverter;
import com.floydwiz.notepad.utils.Status;
import com.floydwiz.notepad.utils.StatusConverter;

import org.jetbrains.annotations.NotNull;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;


@Entity(tableName = "Notes")
public class Note {

    @NonNull
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String title;
    private String body;
    private String time;
    private long reminderTime;
    private int backgroundColor;
    private Boolean isReminderSet = false;

    @TypeConverters(ReminderBasisConverter.class)
    private ReminderBasis reminderBasis = ReminderBasis.ONCE;

    @TypeConverters(StatusConverter.class)
    private Status status = Status.ACTIVE;

    @Ignore
    public Note() {

    }

    public Note(@NotNull int id, String title, String body, String time, int backgroundColor, Status status) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.time = time;
        this.backgroundColor = backgroundColor;
        this.status = status;
    }

    @Ignore
    public Note(String title, String body, String time, Status status, long remainingTimeToRemind) {
        this.title = title;
        this.body = body;
        this.time = time;
        this.status = status;
        this.reminderTime = remainingTimeToRemind;
        this.isReminderSet = true;
    }

    @Ignore
    public Note(String title, String body, String time) {
        this.title = title;
        this.body = body;
        this.time = time;
    }

    @Ignore
    public Note(int backgroundColor, @NotNull int id, String title, String body) {
        this.backgroundColor = backgroundColor;
        this.id = id;
        this.title = title;
        this.body = body;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Boolean getReminderSet() {
        return isReminderSet;
    }

    public void setReminderSet(Boolean reminderSet) {
        isReminderSet = reminderSet;
    }

    public int getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public long getReminderTime() {
        return reminderTime;
    }

    public void setReminderTime(long reminderTime) {
        this.reminderTime = reminderTime;
    }

    public ReminderBasis getReminderBasis() {
        return reminderBasis;
    }

    public void setReminderBasis(ReminderBasis reminderBasis) {
        this.reminderBasis = reminderBasis;
    }
}

package com.floydwiz.notepad.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.floydwiz.notepad.utils.ReminderBasis;
import com.floydwiz.notepad.utils.ReminderBasisConverter;

import org.jetbrains.annotations.NotNull;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

@Entity(tableName = "ToDos")
public class Todo {

    @NonNull
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String task;
    private boolean isChecked = false;
    private long reminderTime;
    private Boolean isReminderSet = false;

    @TypeConverters(ReminderBasisConverter.class)
    private ReminderBasis reminderBasis = ReminderBasis.ONCE;

    public Todo(@NotNull int id, String task) {
        this.id = id;
        this.task = task;
    }

    @Ignore
    public Todo(String task) {
        this.task = task;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public boolean isChecked() {
        return isChecked;
    }

    @NotNull
    public int getId() {
        return id;
    }

    public void setId(@NotNull int id) {
        this.id = id;
    }

    public long getReminderTime() {
        return reminderTime;
    }

    public void setReminderTime(long reminderTime) {
        this.reminderTime = reminderTime;
    }

    public Boolean getReminderSet() {
        return isReminderSet;
    }

    public void setReminderSet(Boolean reminderSet) {
        isReminderSet = reminderSet;
    }

    public ReminderBasis getReminderBasis() {
        return reminderBasis;
    }

    public void setReminderBasis(ReminderBasis reminderBasis) {
        this.reminderBasis = reminderBasis;
    }

}

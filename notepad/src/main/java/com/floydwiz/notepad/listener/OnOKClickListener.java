package com.floydwiz.notepad.listener;

/**
 * Created by sahil on 16/1/19.
 **/
public interface OnOKClickListener {
    void onOKClicked(long time, String basis,int id);
}

package com.floydwiz.notepad.listener;

public interface OnReminderClickListener {
    void onReminderClick(int id);
}

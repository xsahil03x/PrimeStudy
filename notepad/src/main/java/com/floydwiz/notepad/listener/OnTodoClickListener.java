package com.floydwiz.notepad.listener;

public interface OnTodoClickListener {
    void onItemClick(Boolean checked, int id);
}

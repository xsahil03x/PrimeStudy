package com.floydwiz.notepad.listener;

public interface OnTodoReminderClickListener {
    void onReminderClick(int id);
}

package com.floydwiz.notepad.listener;

/**
 * Created by sahil on 7/1/19.
 **/
public interface OnArchiveClickListener {
    void onArchiveNote(int id);
}

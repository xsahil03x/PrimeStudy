package com.floydwiz.notepad.adapter;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.floydwiz.notepad.R;
import com.floydwiz.notepad.listener.OnArchiveClickListener;
import com.floydwiz.notepad.listener.OnDeleteClickListener;
import com.floydwiz.notepad.listener.OnItemClickListener;
import com.floydwiz.notepad.model.Note;
import com.floydwiz.notepad.utils.Status;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by sahil on 8/1/19.
 **/

class NotepadFilterableViewHolder extends RecyclerView.ViewHolder {

    TextView noteTitle;
    TextView noteTime;
    ImageView deleteNote;
    ImageView archiveNote;

    NotepadFilterableViewHolder(View itemView) {
        super(itemView);
        this.noteTitle = itemView.findViewById(R.id.tvNoteTitle);
        this.noteTime = itemView.findViewById(R.id.tvNoteTime);
        this.deleteNote = itemView.findViewById(R.id.btnDeleteNote);
        this.archiveNote = itemView.findViewById(R.id.btnArchiveNote);
    }
}

public class NotepadFilterableAdapter extends RecyclerView.Adapter<NotepadFilterableViewHolder> implements Filterable {

    private Context mContext;
    private List<Note> mainNotesList;
    private List<Note> mNotes;
    private List<Note> mNotesFiltered;
    private Status noteType;
    private OnDeleteClickListener onDeleteClickListener;
    private OnItemClickListener onItemClickListener;
    private OnArchiveClickListener onArchiveClickListener;

    private static final String TAG = "NotepadViewHolder";


    public NotepadFilterableAdapter(Context context, OnDeleteClickListener onDeleteClickListener, OnItemClickListener onItemClickListener, OnArchiveClickListener onArchiveClickListener, Status noteType) {
        this.mContext = context;
        this.onDeleteClickListener = onDeleteClickListener;
        this.onItemClickListener = onItemClickListener;
        this.onArchiveClickListener = onArchiveClickListener;
        this.noteType = noteType;
    }

    public void addNotesToList(List<Note> notesList) {
        Log.d(TAG, "addNotesToList() called with: notesList = [" + notesList.size() + "]");
        this.mainNotesList = notesList;
        this.mNotes = mainNotesList;
        this.mNotesFiltered = notesList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public NotepadFilterableViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_show_note_item, parent, false);
        NotepadFilterableViewHolder notepadViewHolder = new NotepadFilterableViewHolder(view);
        notepadViewHolder.deleteNote.setOnClickListener(v -> onDeleteClickListener.onDeleteNote(mNotes.get(notepadViewHolder.getAdapterPosition()).getId()));
        notepadViewHolder.archiveNote.setOnClickListener(v -> onArchiveClickListener.onArchiveNote(mNotes.get(notepadViewHolder.getAdapterPosition()).getId()));
        view.setOnClickListener(v -> onItemClickListener.onItemClick(mNotes.get(notepadViewHolder.getAdapterPosition()).getId()));
        return notepadViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull NotepadFilterableViewHolder holder, int position) {
        holder.noteTitle.setText(mNotes.get(position).getTitle());
        holder.noteTime.setText(mNotes.get(position).getTime());
        GradientDrawable mDrawable = (GradientDrawable) mContext.getResources().getDrawable(R.drawable.item_background_gradient);
        mDrawable.setColor(mNotes.get(position).getBackgroundColor());
        holder.itemView.setBackgroundDrawable(mDrawable);
        holder.archiveNote.setRotation(noteType != Status.ACTIVE ? 180 : 0);
    }

    @Override
    public int getItemCount() {
        if (mNotes == null) {
            return 0;
        } else {
            return mNotes.size();
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mNotesFiltered = mainNotesList;
                } else {
                    List<Note> filteredList = new ArrayList<>();
                    for (Note row : mainNotesList) {
                        if (row.getTitle().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }
                    mNotesFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mNotesFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mNotes = (List<Note>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}

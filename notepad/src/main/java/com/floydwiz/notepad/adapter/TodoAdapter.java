package com.floydwiz.notepad.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.floydwiz.notepad.R;
import com.floydwiz.notepad.listener.OnDeleteClickListener;
import com.floydwiz.notepad.listener.OnItemClickListener;
import com.floydwiz.notepad.listener.OnReminderClickListener;
import com.floydwiz.notepad.listener.OnTodoClickListener;
import com.floydwiz.notepad.model.Todo;
import com.google.android.material.checkbox.MaterialCheckBox;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

class TodoViewHolder extends RecyclerView.ViewHolder {

    MaterialCheckBox todo;
    ImageView btnReminder;
    ImageView btnDelete;

    TodoViewHolder(View itemView) {
        super(itemView);
        this.todo = itemView.findViewById(R.id.cbTodo);
        this.btnReminder = itemView.findViewById(R.id.btnReminder);
        this.btnDelete = itemView.findViewById(R.id.btnDelete);
    }
}

public class TodoAdapter extends RecyclerView.Adapter<TodoViewHolder> {

    private Context mContext;
    private List<Todo> mTodos;
    private OnDeleteClickListener onDeleteClickListener;
    private OnReminderClickListener onReminderClickListener;
    private OnTodoClickListener onItemClickListener;

    private static final String TAG = "NotepadViewHolder";


    public TodoAdapter(Context context, OnDeleteClickListener onDeleteClickListener, OnReminderClickListener onReminderClickListener, OnTodoClickListener onItemClickListener) {
        this.mContext = context;
        this.onDeleteClickListener = onDeleteClickListener;
        this.onReminderClickListener = onReminderClickListener;
        this.onItemClickListener = onItemClickListener;
    }

    public void addTodoToList(List<Todo> todoList) {
        this.mTodos = todoList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public TodoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_todo_item, parent, false);
        TodoViewHolder todoViewHolder = new TodoViewHolder(view);
        todoViewHolder.todo.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                buttonView.setPaintFlags(buttonView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else {
                buttonView.setPaintFlags(buttonView.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            }
        });
        todoViewHolder.btnDelete.setOnClickListener(v -> onDeleteClickListener.onDeleteNote(mTodos.get(todoViewHolder.getAdapterPosition()).getId()));
        todoViewHolder.btnReminder.setOnClickListener(v -> onReminderClickListener.onReminderClick(mTodos.get(todoViewHolder.getAdapterPosition()).getId()));
        todoViewHolder.todo.setOnClickListener(v -> onItemClickListener.onItemClick(mTodos.get(todoViewHolder.getAdapterPosition()).isChecked(), mTodos.get(todoViewHolder.getAdapterPosition()).getId()));
        return todoViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull TodoViewHolder holder, int position) {
        holder.todo.setText(mTodos.get(position).getTask());
        holder.todo.setChecked(mTodos.get(position).isChecked());
//        holder.noteTitle.setText(mNotes.get(position).getTitle());
//        holder.noteTime.setText(mNotes.get(position).getTime());
    }

    @Override
    public int getItemCount() {
        if (mTodos == null) {
            return 0;
        } else {
            return mTodos.size();
        }
    }

}

/*
 * Copyright 2018 DevCompat
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.floydwiz.quicklook;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.floydwiz.notepad.ui.Notepad;
import com.floydwiz.quicksearch.QuickLook;
import com.github.amitkma.calculator.Calculator;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.magarex.flashcard.ui.FlashCard;
import com.magarex.screenshot.ScreenShot;
import com.magarex.texttospeech.TextToSpeechView;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class QuickLookService extends Service implements View.OnTouchListener {
    private WindowManager mWindowManager;
    private View mFabMenuView, mFabMenuItemView, mCurrentView;
    private FloatingActionMenu fabMenu;
    private boolean popupVisible = false;
    private FloatingActionButton quickSearchButton,
            notepadButton, flashCardButton,
            textToSpeechButton, calculatorButton,
            screenShotButton;

    private WindowManager.LayoutParams fabMenuItemParams, fabMenuParams;
    private Point mWindowSize;
    private static final int QUICK_SEARCH_VIEW = 101;
    private static final int NOTEPAD_VIEW = 102;
    private static final int FLASHCARD_VIEW = 103;
    private static final int TEXT_TO_SPEECH_VIEW = 104;
    private static final int CALCULATOR_VIEW = 105;
    private static final int SCREENSHOT_VIEW = 106;

    private int currentlyVisible = Integer.MIN_VALUE;

    private static final String TAG = "QuickLookService";

    public QuickLookService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.getAction() != null && intent.getAction().equals("floydwiz_copy")) {
            Log.i(TAG, "onStartCommand: with action of copy");
            if (fabMenu != null) {
                fabMenu.open(true);
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onCreate() {
        super.onCreate();

        setTheme(R.style.ServiceTheme);

        //Inflate the floating view layout we created
        mFabMenuView = LayoutInflater.from(this).inflate(R.layout.layout_prime_study, null);
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        mWindowSize = new Point();
        mWindowManager.getDefaultDisplay().getRealSize(mWindowSize);
        QuickLook quickLook = new QuickLook(this);
        Notepad notepad = new Notepad(this);
        FlashCard flashCard = new FlashCard(this);
        TextToSpeechView textToSpeechView = new TextToSpeechView(this);
        Calculator calculator = new Calculator(this);

        flashCardButton = mFabMenuView.findViewById(R.id.fabFlashCard);
        notepadButton = mFabMenuView.findViewById(R.id.fabNotePad);
        quickSearchButton = mFabMenuView.findViewById(R.id.fabQuickSearch);
        textToSpeechButton = mFabMenuView.findViewById(R.id.fabTextToSpeech);
        calculatorButton = mFabMenuView.findViewById(R.id.fabCalculator);
        screenShotButton = mFabMenuView.findViewById(R.id.fabScreenShot);
        fabMenu = mFabMenuView.findViewById(R.id.floatingActionMenu);
        fabMenu.setClosedOnTouchOutside(true);
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);


        //Create params
        createParams();

        //Add the view to the window
        if (mWindowManager != null) {
            mWindowManager.addView(mFabMenuView, fabMenuParams);
        }

        //Set the close button
        ImageView closeButtonCollapsed = mFabMenuView.findViewById(R.id.close_btn);
        closeButtonCollapsed.setOnClickListener(view -> {
            //close the service and remove the view from the window
            stopSelf();
        });

        fabMenu.setOnMenuToggleListener(opened -> {
            if (!opened && popupVisible)
                removeView(false);
        });


        //Set the QuickSearch clickListener
        quickSearchButton.setOnClickListener(v -> {
            mCurrentView = quickLook.getView();
            if (popupVisible && currentlyVisible != QUICK_SEARCH_VIEW) {
                removeView(false);
                mFabMenuItemView = mCurrentView;
                addView(QUICK_SEARCH_VIEW);
            } else if (currentlyVisible != QUICK_SEARCH_VIEW) {
                mFabMenuItemView = mCurrentView;
                addView(QUICK_SEARCH_VIEW);
            }
        });

        //Set the Notepad clickListener
        notepadButton.setOnClickListener(v -> {
            mCurrentView = notepad.getView();
            if (popupVisible && currentlyVisible != NOTEPAD_VIEW) {
                removeView(false);
                mFabMenuItemView = mCurrentView;
                addView(NOTEPAD_VIEW);
            } else if (currentlyVisible != NOTEPAD_VIEW) {
                mFabMenuItemView = mCurrentView;
                addView(NOTEPAD_VIEW);
            }
        });

        //Set the Flashcard clickListener
        flashCardButton.setOnClickListener(v -> {
            mCurrentView = flashCard.getView();
            if (popupVisible && currentlyVisible != FLASHCARD_VIEW) {
                removeView(false);
                mFabMenuItemView = mCurrentView;
                addView(FLASHCARD_VIEW);
            } else if (currentlyVisible != FLASHCARD_VIEW) {
                mFabMenuItemView = mCurrentView;
                addView(FLASHCARD_VIEW);
            }
        });

        //Set the TextToSpeechView clickListener
        textToSpeechButton.setOnClickListener(v -> {
            mCurrentView = textToSpeechView.getView();
            if (popupVisible && currentlyVisible != TEXT_TO_SPEECH_VIEW) {
                removeView(false);
                mFabMenuItemView = mCurrentView;
                addView(TEXT_TO_SPEECH_VIEW);
            } else if (currentlyVisible != TEXT_TO_SPEECH_VIEW) {
                mFabMenuItemView = mCurrentView;
                addView(TEXT_TO_SPEECH_VIEW);
            }
        });

        //Set the Calculator clickListener
        calculatorButton.setOnClickListener(v -> {
            mCurrentView = calculator.getView();
            if (popupVisible && currentlyVisible != CALCULATOR_VIEW) {
                removeView(false);
                mFabMenuItemView = mCurrentView;
                addView(CALCULATOR_VIEW);
            } else if (currentlyVisible != CALCULATOR_VIEW) {
                mFabMenuItemView = mCurrentView;
                addView(CALCULATOR_VIEW);
            }
        });

        screenShotButton.setOnClickListener(v -> {
            mCurrentView = new ScreenShot(this);
            if (popupVisible && currentlyVisible != SCREENSHOT_VIEW) {
                removeView(false);
                mFabMenuItemView = mCurrentView;
                addView(SCREENSHOT_VIEW);
            } else if (currentlyVisible != SCREENSHOT_VIEW) {
                mFabMenuItemView = mCurrentView;
                addView(SCREENSHOT_VIEW);
            }
        });
    }

    private void createParams() {

        //Params for FabMenu
        fabMenuParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);

        //Specify the view position of FabMenu
        fabMenuParams.gravity = Gravity.TOP | Gravity.END;        //Initially view will be added to top-right corner
        fabMenuParams.x = 0;
        fabMenuParams.y = 25;

        //Params for FabMenuItem
        fabMenuItemParams = new WindowManager.LayoutParams(
                (int) (mWindowSize.x * 0.2),
                (int) (mWindowSize.y * 0.65),
                WindowManager.LayoutParams.TYPE_PHONE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                PixelFormat.TRANSLUCENT);

        //Specify the view position of FabMenuItem
        fabMenuItemParams.gravity = Gravity.TOP | Gravity.START;        //Initially view will be added to top-right corner
        fabMenuItemParams.x = (int) (mWindowSize.x * 0.75);
        fabMenuItemParams.y = (int) (mWindowSize.y * 0.14);
    }

    private void removeView(boolean shouldUnregister) {
        mWindowManager.removeView(mFabMenuItemView);
        mFabMenuItemView = null;
        popupVisible = false;
        currentlyVisible = Integer.MIN_VALUE;
        if (shouldUnregister) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mScreenshotReceiver);
        }
    }

    private void addView(int visibleView) {
        mFabMenuItemView.setOnTouchListener(this);
        if (visibleView != SCREENSHOT_VIEW) {
            mWindowManager.addView(mFabMenuItemView, fabMenuItemParams);
        } else {
            //Params for FabMenuItem
            WindowManager.LayoutParams screenshotViewParams = new WindowManager.LayoutParams(
                    mWindowSize.x,
                    mWindowSize.y,
                    WindowManager.LayoutParams.TYPE_PHONE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                    PixelFormat.TRANSLUCENT);

            //Specify the view position of FabMenuItem
            screenshotViewParams.gravity = Gravity.TOP | Gravity.START;        //Initially view will be added to top-right corner
            screenshotViewParams.x = 0;
            screenshotViewParams.y = 0;
            IntentFilter filter = new IntentFilter("FLOYDWIZ_ACTION_SCREENSHOT_COMPLETED");
            LocalBroadcastManager.getInstance(this).registerReceiver(mScreenshotReceiver, filter);
            mWindowManager.addView(mFabMenuItemView, screenshotViewParams);
        }
        popupVisible = true;
        currentlyVisible = visibleView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mFabMenuView != null && popupVisible) {
            mWindowManager.removeView(mFabMenuView);
            mWindowManager.removeView(mFabMenuItemView);
        } else mWindowManager.removeView(mFabMenuView);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getX() == 0.0 && event.getY() == 0.0) {
            if (v.isAttachedToWindow()) {
                removeView(false);
            }
        }
        return false;
    }

    private BroadcastReceiver mScreenshotReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            removeView(true);
        }
    };
}

/*
 * Copyright 2018 Magarex
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.magarex.texttospeech;

import android.content.Context;
import android.media.MediaPlayer;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.transition.ChangeBounds;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AnticipateOvershootInterpolator;

import com.airbnb.lottie.LottieAnimationView;
import com.floydwiz.shared.AppExecutors;
import com.floydwiz.shared.AppUtils;
import com.floydwiz.shared.ClipBoardManager;
import com.google.android.material.snackbar.Snackbar;
import com.magarex.texttospeech.widget.PlayPauseView;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;

public class TextToSpeechView {

    private Context mContext;
    private TextToSpeech textToSpeechView;
    private MediaPlayer mediaPlayer;
    private String dataToSpeak;
    private PlayPauseView btnPlayPause;
    private LottieAnimationView soundView;
    private ConstraintLayout constraintView;
    private File destinationFile;

    private static final String UTTERANCE_ID = "speech";
    private static final Locale INDIA = new Locale("en", "IN");
    private static final String TAG = "TextToSpeechView";

    public TextToSpeechView(Context mContext) {
        this.mContext = mContext;
    }

    public View getView() {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_main, null);
        btnPlayPause = view.findViewById(R.id.btnPlayPause);
        constraintView = view.findViewById(R.id.main);
        soundView = view.findViewById(R.id.sound_view);
        ClipBoardManager mClipBoardManager = new ClipBoardManager(mContext);
        dataToSpeak = mClipBoardManager.getRetrievedData();

        textToSpeechView = new TextToSpeech(mContext, status -> {
            if (!dataToSpeak.equals("Oops nothing copied")) {
                if (status == TextToSpeech.SUCCESS) {
                    textToSpeechView.setLanguage(INDIA);
                    destinationFile = new File(mContext.getCacheDir(), UTTERANCE_ID + ".wav");
                    textToSpeechView.synthesizeToFile(dataToSpeak, null, destinationFile, UTTERANCE_ID);
                } else {
                    Log.e("TTS", "Initialization failed");
                }
            } else {
                AppUtils.showBasicSnackBar(view, dataToSpeak, Snackbar.LENGTH_SHORT);
            }
        });

        textToSpeechView.setOnUtteranceProgressListener(new UtteranceProgressListener() {
            @Override
            public void onStart(String utteranceId) {
                if (utteranceId.equals(UTTERANCE_ID))
                    AppExecutors.getInstance().getMainThreadExecutor().execute(() -> AppUtils.showBasicSnackBar(view, "Preparing Audio...", Snackbar.LENGTH_INDEFINITE));
            }

            @Override
            public void onDone(String utteranceId) {
                if (utteranceId.equals(UTTERANCE_ID)) {
                    AppExecutors.getInstance().getMainThreadExecutor().execute(() -> AppUtils.showBasicSnackBar(view, "Prepared Successfully", Snackbar.LENGTH_SHORT));
                    initializePlayer(destinationFile.getAbsolutePath());
                }
            }

            @Override
            public void onError(String utteranceId) {
                if (utteranceId.equals(UTTERANCE_ID))
                    AppExecutors.getInstance().getMainThreadExecutor().execute(() -> AppUtils.showBasicSnackBar(view, "Error...", Snackbar.LENGTH_SHORT));
            }
        });

        btnPlayPause.setOnClickListener(v -> {
            if (!dataToSpeak.equals("Oops nothing copied")) {
                if (mediaPlayer != null && !mediaPlayer.isPlaying()) {
                    showVoiceAnim();
                    soundView.playAnimation();
                    btnPlayPause.toggle();
                    mediaPlayer.start();
                } else if (mediaPlayer != null) {
                    btnPlayPause.toggle();
                    soundView.pauseAnimation();
                    mediaPlayer.pause();
                }
            } else AppUtils.showBasicSnackBar(view, dataToSpeak, Snackbar.LENGTH_SHORT);
        });

        view.getViewTreeObserver().addOnWindowAttachListener(new ViewTreeObserver.OnWindowAttachListener() {
            @Override
            public void onWindowAttached() {
                Log.d(TAG, "onWindowAttached() called");
            }

            @Override
            public void onWindowDetached() {
                if (textToSpeechView != null)
                    textToSpeechView.shutdown();
                if (mediaPlayer != null)
                    mediaPlayer.release();
            }
        });

        return view;
    }

    private void showVoiceAnim() {
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(mContext, R.layout.layout_text_to_speech_two);
        Transition transition = new ChangeBounds();
        transition.setInterpolator(new FastOutSlowInInterpolator());
        transition.setDuration(800);
        TransitionManager.beginDelayedTransition(constraintView, transition);
        constraintSet.applyTo(constraintView);
    }

    private void hideVoiceAnim() {
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(mContext, R.layout.layout_text_to_speech);
        Transition transition = new ChangeBounds();
        transition.setInterpolator(new AnticipateOvershootInterpolator(1.0f));
        transition.setDuration(800);
        TransitionManager.beginDelayedTransition(constraintView, transition);
        constraintSet.applyTo(constraintView);
    }

    private void initializePlayer(String path) {
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(path);
            mediaPlayer.prepare();
            mediaPlayer.setOnCompletionListener(player -> {
                if (player != null) {
                    hideVoiceAnim();
                    btnPlayPause.toggle();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
